CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Optional Requirements
* Installation
* Configuration
* Maintainers
* Previous Maintainers

INTRODUCTION
------------

What is Vault for Drupal?

Vault for Drupal is a tool for securely accessing secrets using the Hashicorp
Vault API.

A secret is anything that you want to tightly control access to, such as API
keys, passwords, or certificates.
 
The Vault module provides a client for connecting to A HashiCorp Vault or
The Linux Foundation OpenBao server.

* For the full description of the module visit:
  https://www.drupal.org/project/vault

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/vault

REQUIREMENTS
------------

- An accessible Vault or OpenBao Server.
- csharpru/vault-php (installed by composer)
- A Vault Authentication Strategy plugin.
    - [Token Authentication](https://www.drupal.org/project/vault_auth_token)
    - [AppRole Authentication](https://www.drupal.org/project/vault_auth_approle)

OPTIONAL REQUIREMENTS
---------------------

- [Encrypt](https://www.drupal.org/project/encrypt)

INSTALLATION
------------

Install the Vault module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

Install an Authentication Strategy and configure as documented by the strategy.

CONFIGURATION
-------------

1. Verify you have an Authentication Strategy installed.
2. Navigate to admin/config/system/vault.
3. Provide the URL for accessing your Vault server on the settings form or in
   your settings.php file.
4. Configure the Authentication Strategy.
5. Optionally: Configure an alternative Lease Storage plugin. The default module
   'Drupal State' stores leases in the Drupal State. If you have the optional
   Encrypt module you may use the "Encrypted State" plugin to avoid plain-text
   credentials stored in the database. Encrypted State paired with the
   vault_transit_encrypt encryption provider allows for the burden of 
   cryptographic key management to be centralized in Vault.
6. Save the configuration.
7. Validate no errors are displayed when viewing admin/reports/status.

MAINTAINERS
-----------

* Conrad Lara - https://www.drupal.org/u/cmlara

PREVIOUS MAINTAINERS
--------------------
Nick Santamaria - https://www.drupal.org/u/nicksanta
