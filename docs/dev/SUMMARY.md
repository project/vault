* [Authentication Strategy Plugins](auth-plugins.md)
* [Secret Engines](secret-engines.md)
* [Lease Storage Plugin](lease-storage-plugins.md)
