# Vault Client Configuration

## Location
The Vault Client configuration form may be found at `admin/config/system/vault`.

## Cache

### Default Cache Provider

The default cache is a PHPArray.

During normal Drupal operations (php-fpm/cli) this array will be destroyed at
the close of each request.

See [Client Cache Storage](client-cache-storage.md) for information on 
replacing the default cache storage.

### Auth Token

The validated authentication token is stored in the cache. The TTL of the
auth token is controlled by the Vault API reported token lease duration.

### Cache TTL

To decrease network latency and load on the Vault API server the client can
cache reads. Cached keys will not be read from the Vault server. The Vault
client has no knowledge of changes inside the Vault server and can serve
'stale' results.

A value of '0' disables the client read cache.

### Clear Cache

Clears all entries (including Auth Token) from the client cache.

The cache is also cleared when the Drupal Cache is flushed.
