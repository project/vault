<!-- cspell:ignore rwxrwxrwx -->
# Client Cache Storage

## Default Storage

The default storage is backed by
`Symfony\Component\Cache\Adapter\ArrayAdapter` and is not persistent beyond
the lifetime of a process execution.

```yml title="Default 'vault.cache' service"
 services:
  vault.cache:
    class: Symfony\Component\Cache\Adapter\ArrayAdapter
    public: false
```
The `vault.cache` service is declared `public: false` to reduce the attack
surface of the cache on a deployed site. This setting requires the service to
be directly injected into services preventing calling through 
`\Drupal::service('vault.cache')`. The `vault.cache` is intended only for 
use by the module processes. 


## Reasons to Replace the Storage

Sites owners may wish to replace the memory storage to decrease request 
latency and Vault server load.

Currently the underlying client requires a cached authentication attempt 
that resolves authentication credentials into an operational Token credential, 
(this requirement exists even with the token authentication strategy).
 
## Security Considerations

The cache service will always be provided a cleartext copy of the validated
authentication token to store.

If read caching is enabled ([Configuration Cache TTL](vault-config-form.md#cache-ttl))
the cache will additionally contain copies of Vault read requests that may
contain cleartext secrets.

The client cache should be protected according to industry standards for
protecting sensitive data.


### Common security practices

* The cache should not be placed inside the server document root.
* Cache should not be accessible to all users/process on the server.
* The `vault.cache` (and any dependency services) should have limited 
  container visibility (set `public: false`).
* Token max lifetime should be limited to mitigate cache exposure.


## Replacing the Storage Backend

Drupal Core provides a method for site owners to provide custom container
overrides that may be utilized to replace the default cache with a custom
provider.


### Requirements

Any PSR-6 provider that implements `Psr\Cache\CacheItemPoolInterface` may be
used as the `vault.cache`.


### Known cache providers

Known cache providers include:

* [Symfony Cache](https://symfony.com/doc/current/components/cache.html#available-cache-adapters)
* [Laminas Cache](https://docs.laminas.dev/laminas-cache/v4/storage/adapter/)
* [Scrapbook](https://www.scrapbook.cash/adapters/)

More providers may be found by [Packagist: packages implementing psr/cache-implementation](https://packagist.org/providers/psr/cache-implementation).


### Pruneable caches

The module cron hook will attempt to prune caches implementing `Symfony\Component\Cache\PruneableInterface`


### Custom container services file

Custom services files are YAML files that conform to the Drupal Core 
Service File structure.
See [Structure of a service file](https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/structure-of-a-service-file)
for more information.

Entries placed in these files may override or decorate existing services
declared by modules.


### Core settings.php requirements.

```php title="Default container_yamls definition in settings.php"
/**
 * Load services definition file.
 */
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
```

Drupal includes in the default settings.php a `$settings['container_yamls']`
definition that will process a `services.yml` located in the site directory. 
Additional array entries may be added to process additional files.

If your site does not have a valid `container_yamls` specification add one 
referencing your custom container services file.


### PSR-16 to PSR-6 conversion

Many packages include adapters to allow use of PSR-16 caches in systems that
only support PSR-6. Check your cache package for details on if it is PSR-6
or PSR-16 and if it provides an adapter class.

```yaml title="Sample conversion from PSR-16 to PSR-6 using Symfony Psr16Adapter"
services:
 vault.some.psr16.cache:
  class: \Example\Psr16\Cache
  public: false
 vault.cache:
  class: Symfony\Component\Cache\Adapter\Psr16Adapter
  public: false
  arguments: 
    - '@vault.some.psr16.cache'
```


### Backend notes


##### :warning: File based caches

The symfony/cache package does not directly attempt to protect written files 
from being world readable (-rwxrwxrwx). Filesystem permissions depend upon 
the process umask() to limit the creation of world readable files.

When using a Symfony file based cache it may be advisable to add a `umask()` 
call to the start of the site `settings.php`.
```php title="Recommended umask() call for settings.php"
<?php
# Remove -------rwx for new files/directories to protect `vault.cache`
@umask(0007);
```
Other file based cache may have similar expectations of security. It is the 
site owner responsibility to validate file permissions before deploying any
cache replacement.


### Sample 'vault.cache' replacements


#### Symfony FileSystemAdapter

:warning: Review [File based Caches](#file-based-caches) prior to deployment.

```yaml title="Example persistent cache storage using Symfony FilesystemAdapter"
services:
  # Ensure server umask is set to at least 0007.
  vault.cache:
    class: Symfony\Component\Cache\Adapter\FilesystemAdapter
    public: false
    arguments:
      # Vault namespace
      - 'vault'
      # Default lifetime of 2 hours if a lifetime not provided with value set.
      # Note: Generally the underlying API module sets a TTL based on either
      # the token lifetime or the configured Read Cache TTL.
      - 7200,
      # Store cache in /tmp/vault_client_storage
      - '/tmp/vault_client_storage'
```
