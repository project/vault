# Vault for Drupal

This site covers setup and developer docs for the following Drupal projects:

* [drupal.org/project/vault](https://www.drupal.org/project/vault)
* [drupal.org/project/vault_key_kv](https://www.drupal.org/project/vault_key_kv)
* [drupal.org/project/encrypt_vault_transit](https://www.drupal.org/project/encrypt_vault_transit)
* [drupal.org/project/vault_key_aws](https://www.drupal.org/project/vault_key_aws)
* [drupal.org/project/vault_auth_token](https://www.drupal.org/project/vault_auth_token)

## What is Vault for Drupal?

Vault for Drupal is a tool for securely accessing secrets using the HashiCorp
Vault API.

A secret is anything that you want to tightly control access to, such as API
keys, passwords, or certificates.

## What are HashiCorp Vault and The Linux Foundation OpenBao?
HashiCorp Vault is a source available (BSL license) project.  
The Linux Foundation OpenBao is an open source (MIT license) fork of
HashiCorp Vault.

Both provide secure storage of secrets outside of Drupal.

### HashiCorp Vault
* [What is Vault?](https://www.vaultproject.io/intro/index.html)
* [Common Use Cases](https://www.vaultproject.io/intro/use-cases.htm)

### The Linux Foundation OpenBao
* [What is OpenBao?](https://openbao.org/docs/what-is-openbao/)
* [Common Use Cases](https://openbao.org/docs/use-cases/)

## Why Vault/OpenBao with Drupal?

**Unparalleled Feature-Set**

Vault and OpenBao have a significant range of features for storing secrets in a
secure manner:

Some features include:
* Encrypted key/value storage
* Encryption-as-a-service
* Automatic rotation of credentials
* Revocation of credentials
* Audit logging for compliance and intrusion detection 

**Free and Open Source Software**

The Drupal community has produced some excellent tooling to abstract [secret storage](https://www.drupal.org/project/key) and [encryption](https://www.drupal.org/project/encrypt). However there are issues with the ecosystem of tools which leverage these abstractions to perform the cryptographic functions.

* Most of the existing integrations are for commercial services
* The FOSS options are difficult to operate in a secure manner
