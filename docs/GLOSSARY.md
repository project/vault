## encryption method
Plugin manager provided by [Encrypt](https://www.drupal.org/project/encrypt) which handles cryptographic functions on data.

## FOSS
Free and Open Source Software

## HashiCorp
Commercial entity behind Vault project

## HashiCorp Vault
HashiCorp Vault is an identity-based secrets and encryption management system.

HashiCorp Vault is the project that created the Vault API and associated
standards.

HashiCorp Vault is available for use on-premise/cloud as a self-managed
solutions or as a managed service through the HashiCorp Cloud Platform.

## key storage provider
Plugin manager provided by [Key](https://www.drupal.org/project/key) module which controls where sensitive data is stored and retrieved.

## Linux Foundation, The
The Linux Foundation is a 501(c)(6) non-profit that provides a neutral,
trusted hub for developers and organizations to code, manage, and scale open
technology projects and ecosystems.

Coordinates the OpenBao fork of HashiCorp Vault.

## OpenBao
The Linux Foundation OpenBao is an identity-based secrets and encryption
management system.

OpenBao is an API computable fork of HashiCorp Vault licensed under the Mozilla
Public License (MPL) 2.0.  Based on the last MPL commit of Vault before
HashiCorp converted to the Business Source License (BSL).

## transit secret engine
Encryption-as-a-service feature of Vault - https://www.vaultproject.io/docs/secrets/transit/index.html
