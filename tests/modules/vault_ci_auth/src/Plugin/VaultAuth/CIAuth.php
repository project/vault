<?php

namespace Drupal\vault_ci_auth\Plugin\VaultAuth;

use Drupal\vault\Plugin\VaultAuthBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;

/**
 * Provides a static token for use in CI labs.
 *
 * @VaultAuth(
 *   id = "ciauth",
 *   label = "Static token for CI environment",
 *   description = @Translation("For CI use only. Not for production."),
 * )
 */
final class CIAuth extends VaultAuthBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationStrategy(): TokenAuthenticationStrategy {
    return new TokenAuthenticationStrategy('myroot');
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

}
