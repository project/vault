<?php

namespace Drupal\vault_ci_lease_form\Plugin\VaultLeaseStorage;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vault\Plugin\VaultLeaseStorageBase;
use Drupal\vault\Plugin\VaultPluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a static token for use in CI labs.
 *
 * @VaultLeaseStorage (
 *   id = "cileaseform",
 *   label = "Static lease plugin for CI environment with form",
 *   description = @Translation("For CI use only. Not for production."),
 * )
 * @codeCoverageIgnore
 */
final class CILeaseForm extends VaultLeaseStorageBase implements VaultPluginFormInterface, ConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    // During initial setup or if credentials are invalid we do not have a
    // client available.
    try {
      $vault_client = $container->get('vault.vault_client_no_lease_storage');
    }
    catch (\Exception $e) {
      $vault_client = NULL;
    }

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $vault_client,
      $container->get('logger.channel.vault'),
    );
  }

  /**
   * A mapping of valid lease data.
   *
   * This can be used for any mocks that require access to test variables.
   *
   * @var array
   */
  protected array $leaseDataLookup = [
    'valid_lease_1' => [
      'lease_id' => 'rabbitmq1',
      'data' => [
        'username' => 'username',
        'password' => 'password',
      ],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function getLeaseRaw(string $storage_key): ?array {
    if (isset($this->leaseDataLookup[$storage_key])) {
      return $this->leaseDataLookup[$storage_key]['lease_id'];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllLeases(): array {
    return $this->leaseDataLookup;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeaseId(string $storage_key): string|FALSE {
    if (isset($this->leaseDataLookup[$storage_key])) {
      return $this->leaseDataLookup[$storage_key];
    }

    return FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function deleteLease(string $storage_key): void {
    // noop.
  }

  /**
   * {@inheritdoc}
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void {
    // noop.
  }

  /**
   * {@inheritdoc}
   */
  public function updateLeaseExpires(string $storage_key, int $new_expires, bool $renewable): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['test_lease_plugin_field'] = [
      '#type' => 'textfield',
      '#title' => 'Test string field',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $value = $form_state->getValue('test_lease_plugin_field');

    if ($value == 'set_error') {
      $form_state->setErrorByName('test_lease_plugin_field', 'simulated error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['test_lease_plugin_field' => ''];
  }

}
