<?php

namespace Drupal\vault_ci_state\Plugin\VaultLeaseStorage;

use Drupal\vault\Plugin\VaultLeaseStorageBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a static token for use in CI labs.
 *
 * @VaultLeaseStorage (
 *   id = "cilease",
 *   label = "Static state for CI environment",
 *   description = @Translation("For CI use only. Not for production."),
 * )
 */
final class CILease extends VaultLeaseStorageBase {

  /**
   * A mapping of valid lease data.
   *
   * This can be used for any mocks that require access to test variables.
   *
   * @var array
   */
  protected array $leaseDataLookup = [
    'valid_lease_1' => [
      'lease_id' => 'rabbitmq1',
      'data' => [
        'username' => 'username',
        'password' => 'password',
      ],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    // During initial setup or if credentials are invalid we do not have a
    // client available.
    try {
      $vault_client = $container->get('vault.vault_client_no_lease_storage');
    }
    catch (\Exception $e) {
      $vault_client = NULL;
    }

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $vault_client,
      $container->get('logger.channel.vault'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeaseRaw(string $storage_key): ?array {
    if (isset($this->leaseDataLookup[$storage_key])) {
      return $this->leaseDataLookup[$storage_key]['lease_id'];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllLeases(): array {
    return $this->leaseDataLookup;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeaseId(string $storage_key): string|FALSE {
    if (isset($this->leaseDataLookup[$storage_key])) {
      return $this->leaseDataLookup[$storage_key];
    }

    return FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function deleteLease(string $storage_key): void {
    // noop.
  }

  /**
   * {@inheritdoc}
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void {
    // noop.
  }

  /**
   * {@inheritdoc}
   */
  public function updateLeaseExpires(string $storage_key, int $new_expires, bool $renewable): bool {
    return TRUE;
  }

}
