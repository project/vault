<?php

namespace Drupal\vault_ci_auth_form\Plugin\VaultAuth;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vault\Plugin\VaultAuthBase;
use Drupal\vault\Plugin\VaultPluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;

/**
 * Provides a static token for use in CI labs.
 *
 * @VaultAuth(
 *   id = "ciauthform",
 *   label = "Static token for CI environment with form",
 *   description = @Translation("For CI use only. Not for production."),
 * )
 *
 * @codeCoverageIgnore
 */
final class CIAuthForm extends VaultAuthBase implements VaultPluginFormInterface, ConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationStrategy(): TokenAuthenticationStrategy {
    return new TokenAuthenticationStrategy('myroot');
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['test_auth_plugin_field'] = [
      '#type' => 'textfield',
      '#title' => 'Test string field',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $value = $form_state->getValue('test_auth_plugin_field');

    if ($value == 'set_error') {
      $form_state->setErrorByName('test_auth_plugin_field', 'simulated error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['test_auth_plugin_field' => ''];
  }

}
