<?php

namespace Drupal\Tests\vault\Traits\Constraint;

use PHPUnit\Framework\Constraint\Constraint;

/**
 * Require that a string is a key in an array of strings.
 *
 * @no-named-arguments Parameter names are not covered by the backward compatibility promise for PHPUnit
 */
final class StringIsOneOf extends Constraint {
  /**
   * Array of strings to accept.
   *
   * @var string[]
   */
  private array $array;

  /**
   * Construct a new StringIsOneOf constraint object.
   */
  public function __construct(array $array) {
    $this->array = $array;
  }

  /**
   * Returns a string representation of the constraint.
   */
  public function toString(): string {

    return sprintf(
          'is one of "%s"',
          implode(', ', $this->array)
      );
  }

  /**
   * Evaluates the constraint is matched.
   *
   * @param mixed $other
   *   Value or object to evaluate.
   *
   * @returns
   *   Returns true if the constraint is met, false otherwise.
   */
  protected function matches($other): bool {
    if (!is_string($other)) {
      return FALSE;
    }

    return in_array($other, $this->array);
  }

}
