<?php

namespace Drupal\Tests\vault\Kernel;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests the 'vault.settings' form.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Form\VaultConfigForm
 * @codeCoverageIgnore
 *
 * cspell::ignore NOBLANKS
 */
class VaultConfigFormKernelTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'vault',
    'vault_ci_auth_form',
    'vault_ci_auth',
    'vault_ci_lease_form',
    'vault_ci_state',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig('vault');
  }

  /**
   * Test form route permissions.
   */
  public function testFormRoutePermissions(): void {

    $http_kernel = $this->container->get('http_kernel');

    $request = Request::create('admin/config/system/vault');
    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(HtmlResponse::class, $response);
    $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode(), 'Access forbidden for anonymous user');

    // Create and log in a user.
    $this->setUpCurrentUser(['uid' => 2], ['administer vault']);

    $request = Request::create('admin/config/system/vault');
    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(HtmlResponse::class, $response);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode(), 'Access granted for user with permissions');
  }

  /**
   * Test VaultConfigForm.
   */
  public function testVaultConfigForm(): void {

    // Create and log in a user.
    $this->setUpCurrentUser(['uid' => 2], ['administer vault']);
    $http_kernel = $this->container->get('http_kernel');

    $request = Request::create('admin/config/system/vault');

    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(HtmlResponse::class, $response);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    $this->assertIsString($response->getContent());
    $document = new \DOMDocument();
    @$document->loadHTML($response->getContent(), LIBXML_NOBLANKS);
    $xpath = new \DOMXPath($document);

    $form_token = $this->getFormAttributeValue($xpath, 'form_token');
    $this->assertNotNull($form_token);

    $form_build_id = $this->getFormAttributeValue($xpath, 'form_build_id');
    $this->assertNotNull($form_build_id);

    $settings = [
      'base_url' => "http://vault:8200",
      'lease_ttl_increment' => "7200",
      'read_cache_ttl' => "3600",
      'plugin_auth' => "ciauthform",
      'plugin_lease_storage' => "state",
      'form_build_id' => $form_build_id,
      'form_token' => $form_token,
      'form_id' => "vault.config_form",
      '_triggering_element_name' => "plugin_auth",
      '_drupal_ajax' => 1,
    ];

    $request = Request::create('admin/config/system/vault?ajax_form=1&_wrapper_format=drupal_ajax', 'POST', $settings, [], [], ['HTTP_ACCEPT' => 'application/json']);
    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(AjaxResponse::class, $response);
    $this->assertIsString($response->getContent());
    $data = json_decode($response->getContent(), TRUE);
    $this->assertIsArray($data, 'Response data decoded');
    $ajax_data = $this->getAjaxResponseByCommand($data, 'insert');
    $this->assertNotEmpty($ajax_data);
    $this->assertStringContainsString('name="plugin_auth_settings[test_auth_plugin_field]"', $ajax_data['data']);

    // Test the lease plugin forms.
    $ajax_data = $this->getAjaxResponseByCommand($data, 'update_build_id');
    $this->assertNotEmpty($ajax_data);
    $settings['form_build_id'] = $ajax_data['new'];
    $settings['plugin_lease_storage'] = "cileaseform";
    $settings['_triggering_element_name'] = "plugin_lease_storage";

    $request = Request::create('admin/config/system/vault?ajax_form=1&_wrapper_format=drupal_ajax', 'POST', $settings, [], [], ['HTTP_ACCEPT' => 'application/json']);
    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(AjaxResponse::class, $response);
    $this->assertIsString($response->getContent());
    $data = json_decode($response->getContent(), TRUE);
    $this->assertIsArray($data, 'Response data decoded');
    $ajax_data = $this->getAjaxResponseByCommand($data, 'insert');
    $this->assertNotEmpty($ajax_data);
    $this->assertStringContainsString('name="lease_storage_plugin_config[test_lease_plugin_field]"', $ajax_data['data']);

    // Test submit form.
    $ajax_data = $this->getAjaxResponseByCommand($data, 'update_build_id');
    $this->assertNotEmpty($ajax_data);
    $settings['form_build_id'] = $ajax_data['new'];
    $settings['op'] = 'Save configuration';
    unset($settings['_drupal_ajax']);
    unset($settings['_triggering_element_name']);
    $settings['plugin_auth_settings']['test_auth_plugin_field'] = 'auth plugin config data';
    $settings['lease_storage_plugin_config']['test_lease_plugin_field'] = 'lease storage data';

    $request = Request::create('admin/config/system/vault', 'POST', $settings);
    $response = $http_kernel->handle($request);
    $this->assertInstanceOf(LocalRedirectResponse::class, $response);
    $this->assertEquals(Response::HTTP_SEE_OTHER, $response->getStatusCode());

    /** @var \Drupal\vault\VaultConfigInterface $vault_config */
    $vault_config = $this->container->get('vault.config');

    $this->assertEquals('http://vault:8200', $vault_config->getBaseUrl(), 'BaseURL set correctly');
    $this->assertEquals(7200, $vault_config->getLeaseTtlIncrement(), 'TTL renew time set correctly');
    $this->assertEquals('ciauthform', $vault_config->getAuthPluginName(), 'Auth plugin name set correctly');
    $this->assertEquals('cileaseform', $vault_config->getLeasePluginName(), 'Lease plugin name set correctly');
    $this->assertEquals(['test_auth_plugin_field' => 'auth plugin config data'], $vault_config->getAuthPluginConfig(), 'Auth plugin config set correctly');
    $this->assertEquals(['test_lease_plugin_field' => 'lease storage data'], $vault_config->getLeasePluginConfig(), 'Lease plugin config set correctly');
  }

  /**
   * Searches input for field with a specific 'name' value.
   *
   * @param \DOMXPath $xpath
   *   The DOMXPath to use for searching.
   * @param string $attribute_name
   *   The value to search for of name attribute.
   *
   * @return string|null
   *   Value of the field with matching attribute.
   */
  protected function getFormAttributeValue(\DOMXPath $xpath, string $attribute_name): ?string {
    $query = $xpath->query("//input[@name='$attribute_name']");
    if ($query == NULL || $query->count() != 1) {
      return NULL;
    }
    $dom = $query->item(0);
    if (!$dom instanceof \DOMElement) {
      return NULL;
    }
    return $dom->getAttribute('value');
  }

  /**
   * Get the data from a specific ajax response command.
   *
   * @param array $ajax_response
   *   An array of ajax response commands.
   * @param string $command
   *   The command to return data for.
   *
   * @return array|null
   *   The response data, or null.
   */
  protected function getAjaxResponseByCommand(array $ajax_response, string $command): ?array {
    foreach ($ajax_response as $data) {
      if (!is_array($data)) {
        continue;
      }
      if (!empty($data['command'] && $data['command'] == $command)) {
        return $data;
      }
    }
    return NULL;
  }

}
