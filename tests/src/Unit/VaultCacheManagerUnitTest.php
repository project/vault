<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Tests\vault\Unit\Mocks\TestPruneableCacheInterface;
use Drupal\vault\VaultCacheManager;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Tests the VaultCacheManager custom functions.
 *
 * @group vault
 *
 * @coversDefaultClass \Drupal\vault\VaultCacheManager
 * @codeCoverageIgnore
 */
class VaultCacheManagerUnitTest extends UnitTestCase {

  /**
   * Test cache clear.
   *
   * @covers ::clearCache
   */
  public function testCacheClear(): void {
    $cache = $this->createMock(CacheItemPoolInterface::class);
    $cache
      ->expects($this->exactly(2))
      ->method('clear')
      ->willReturn(TRUE, FALSE);
    $manager = new VaultCacheManager($cache);
    $this->assertTrue($manager->clearCache());
    $this->assertFalse($manager->clearCache());
  }

  /**
   * Test cache prune.
   *
   * @covers ::pruneCache
   */
  public function testPruneCache(): void {
    $cache = $this->createMock(TestPruneableCacheInterface::class);
    $cache
      ->expects($this->exactly(2))
      ->method('prune')
      ->willReturn(TRUE, FALSE);
    $manager = new VaultCacheManager($cache);
    $this->assertTrue($manager->pruneCache());
    $this->assertFalse($manager->pruneCache());

    $cache = $this->createMock(CacheItemPoolInterface::class);
    $manager = new VaultCacheManager($cache);
    $this->assertTrue($manager->pruneCache());
  }

}
