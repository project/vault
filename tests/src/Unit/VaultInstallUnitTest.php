<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\VaultClientInterface;
use Drupal\vault\VaultConfigInterface;
use Vault\ResponseModels\Response;

// We need the install.inc constants.
require __DIR__ . '/../../../../../../core/includes/install.inc';

// The methods under test.
require __DIR__ . '/../../../vault.install';

/**
 * Tests the Vault Client Factory.
 *
 * @group vault
 *
 * @covers ::vault_requirements
 * @codeCoverageIgnore
 */
class VaultInstallUnitTest extends UnitTestCase {

  /**
   * Test vault_requirements().
   */
  public function testVaultRequirements(): void {

    $logger_mock = $this->createMock(LoggerChannelInterface::class);
    $logger_factory_mock = $this->createMock(LoggerChannelFactoryInterface::class);
    $logger_factory_mock->method('get')->willReturn($logger_mock);

    $vault_config_mock = $this->createMock(VaultConfigInterface::class);
    $vault_config_mock->method('getAuthPluginName')->willReturnOnConsecutiveCalls('UnitTestPlugin', NULL);

    $vault_response = new Response();
    $vault_client_mock = $this->createMock(VaultClientInterface::class);
    $vault_client_mock->method('get')->willReturnOnConsecutiveCalls(
      $vault_response,
      $this->throwException(new \Exception('simulated fault')),
    );

    $container = new ContainerBuilder();
    $container->set('vault.config', $vault_config_mock);
    $container->set('logger.factory', $logger_factory_mock);
    $container->set('vault.vault_client', $vault_client_mock);
    \Drupal::setContainer($container);

    $result = vault_requirements('runtime');
    $this->assertNotEmpty($result);
    $this->assertEquals(REQUIREMENT_OK, $result['vault-sdk']['severity'], "AWS SDK detected'");
    $this->assertEquals(REQUIREMENT_OK, $result['vault-auth-creds']['severity'], 'Status ok when plugin configured');
    $this->assertEquals(REQUIREMENT_OK, $result['vault-auth-ping']['severity'], 'Simulated auth ping successful');

    $result = vault_requirements('runtime');
    $this->assertEquals(REQUIREMENT_ERROR, $result['vault-auth-creds']['severity'], 'Error shown when no plugin configured');
    $this->assertEquals(REQUIREMENT_ERROR, $result['vault-auth-ping']['severity'], 'Simulated auth ping failure');

    $result = vault_requirements('install');
    $this->assertEmpty($result, 'No install checks exist');
  }

}
