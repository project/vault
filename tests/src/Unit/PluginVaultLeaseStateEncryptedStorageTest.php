<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\encrypt\EncryptionProfileInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseEncryptedStateStorage;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the VaultLeaseStateStorage plugin.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseEncryptedStateStorage
 * @covers \Drupal\vault\Plugin\VaultLeaseStorageBase
 * @codeCoverageIgnore
 */
class PluginVaultLeaseStateEncryptedStorageTest extends VaultLeaseStoragePluginBase {

  /**
   * Mock of the K/V Storage that supports expiration time.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stateMock;

  /**
   * Setup the storage mock for use with the tests.
   */
  public function setUp(): void {
    parent::setUp();

    $plugin_config = [
      'encryption_profile' => 'loopback_profile',
    ];

    $encryption_profile_mock = $this->createMock(EncryptionProfileInterface::class);
    $encryption_profile_mock->method('id')
      ->willReturn('loopback_profile');
    $encryption_profile_mock->method('label')
      ->willReturn('Loopback Profile');

    $encryption_entity_mock = $this->createMock(EntityStorageInterface::class);
    $encryption_entity_mock->method('load')
      ->with('loopback_profile')
      ->willReturn($encryption_profile_mock);
    $encryption_entity_mock->method('loadMultiple')
      ->willReturn([$encryption_profile_mock]);

    $entity_type_manager_mock = $this->createMock(EntityTypeManagerInterface::class);

    $entity_type_manager_mock->method('getStorage')
      ->with('encryption_profile')
      ->willReturn($encryption_entity_mock);

    $encryption_service_mock = $this->createMock(EncryptServiceInterface::class);
    $encryption_service_mock->method('encrypt')
      ->willReturnArgument(0);
    // Normally the data is json encoded.
    $encryption_service_mock->method('decrypt')
      ->willReturnArgument(0);

    $storage_get_map = [
      [
        self::NONEXISTENT_LEASE_HASHED,
        NULL,
        NULL,
      ],
      [
        self::VALID_LEASE_1_HASHED,
        NULL,
        json_encode($this->leaseDataLookup['valid_lease_1']),
      ],
    ];

    $this->stateMock = $this->createMock(KeyValueStoreExpirableInterface::class);

    $this->stateMock->expects($this->any())
      ->method('get')
      ->willReturnMap($storage_get_map);
    $this->stateMock->method('getAll')
      ->willReturn([self::VALID_LEASE_1_HASHED => json_encode($this->leaseDataLookup['valid_lease_1'])]);

    $logger_mock = $this->createMock(LoggerInterface::class);

    $this->plugin = new VaultLeaseEncryptedStateStorage($plugin_config, 'state', [], $this->clientMock, $logger_mock, $this->getStringTranslationStub(), $this->stateMock, $entity_type_manager_mock, $encryption_service_mock);
  }

  /**
   * Test create() method.
   */
  public function testCreate(): void {

    $kv_factory_mock = $this->createMock(KeyValueFactoryInterface::class);
    $kv_factory_mock->method('get')
      ->willReturn($this->stateMock);

    $entity_type_manager_mock = $this->createMock(EntityTypeManagerInterface::class);

    $encryption_service_mock = $this->createMock(EncryptServiceInterface::class);

    $client_mock = $this->createMock(VaultClientInterface::class);
    $logger_mock = $this->createMock(LoggerInterface::class);

    $service_map = [
      [
        'vault.vault_client_no_lease_storage',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $client_mock,
      ],
      [
        'logger.channel.vault',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $logger_mock,
      ],
      [
        'string_translation',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->getStringTranslationStub(),
      ],
      [
        'keyvalue.expirable',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $kv_factory_mock,
      ],
      [
        'entity_type.manager',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $entity_type_manager_mock,
      ],
      [
        'encryption',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $encryption_service_mock,
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = VaultLeaseEncryptedStateStorage::create($container_mock, [], 'state', []);
    $this->assertInstanceOf(VaultLeaseEncryptedStateStorage::class, $plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function testSetLease(): void {

    // Json encode the data for comparison as we encode before encrypting.
    $this->stateMock->expects($this->exactly(2))
      ->method('setWithExpire')
      ->with(self::VALID_LEASE_1_HASHED, json_encode($this->leaseDataLookup['valid_lease_1']), 86400);

    parent::testSetLease();
  }

  /**
   * {@inheritdoc}
   */
  public function testUpdateLeaseExpires(): void {

    $this->stateMock->expects($this->exactly(1))
      ->method('setWithExpire')
      ->with(self::VALID_LEASE_1_HASHED, json_encode($this->leaseDataLookup['valid_lease_1']), 86400);

    parent::testUpdateLeaseExpires();
  }

}
