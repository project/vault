<?php

declare(strict_types=1);

namespace Drupal\Tests\vault\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\vault\Logging\VaultLogLevelFilter;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Tests the Vault log level filter.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Logging\VaultLogLevelFilter
 * @codeCoverageIgnore
 */
class VaultLogLevelFilterUnitTest extends UnitTestCase {

  /**
   * Available known log levels.
   *
   * @var string[]
   */
  private const LOG_LEVELS = [
    LogLevel::DEBUG,
    LogLevel::INFO,
    LogLevel::NOTICE,
    LogLevel::WARNING,
    LogLevel::ERROR,
    LogLevel::CRITICAL,
    LogLevel::ALERT,
    LogLevel::EMERGENCY,
  ];

  /**
   * Logger service mock.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $loggerMock;

  /**
   * Setup test data.
   */
  public function setUp(): void {
    parent::setUp();
    $this->loggerMock = $this->createMock(LoggerInterface::class);
  }

  /**
   * Test configured with unknown log level.
   */
  public function testConfigureUnknownLogLevel(): void {
    $this->loggerMock
      ->expects($this->never())
      ->method('log');
    $this->expectException(\InvalidArgumentException::class);
    new VaultLogLevelFilter($this->loggerMock, 'unknown');
  }

  /**
   * Test level filters.
   *
   * @param string $min_level
   *   Minimum (most verbose) log level.
   * @param int $expected_calls
   *   Count $logger::log() expected to be called.
   *
   * @dataProvider providerTestLogFilter
   */
  public function testLogFilter(string $min_level, int $expected_calls): void {
    $stringable_message = new StringableText('Sample log message');
    $this->loggerMock
      ->expects($this->exactly($expected_calls))
      ->method('log')
      ->with(
        $this->isType('string'),
        $this->logicalOr(
          $this->equalTo('Sample log message'),
          $this->equalTo($stringable_message)
        ),
        $this->equalTo(['context' => 'data'])
      );
    $logger = new VaultLogLevelFilter($this->loggerMock, $min_level);
    foreach (self::LOG_LEVELS as $level) {
      $logger->$level("Sample log message", ['context' => 'data']);
      $logger->$level($stringable_message, ['context' => 'data']);
      $logger->log($level, "Sample log message", ['context' => 'data']);
      $logger->log(new StringableText($level), $stringable_message, ['context' => 'data']);
    }
  }

  /**
   * Provider for testLogFilter().
   */
  public static function providerTestLogFilter(): \Generator {
    $tests_per_level = 4;
    yield 'Log Level Debug' => [
      'min_level' => 'debug',
      'expected_calls' => $tests_per_level * 8,
    ];

    yield 'Log Level info' => [
      'min_level' => 'info',
      'expected_calls' => $tests_per_level * 7,
    ];

    yield 'Log Level notice' => [
      'min_level' => 'notice',
      'expected_calls' => $tests_per_level * 6,
    ];

    yield 'Log Level warning' => [
      'min_level' => 'warning',
      'expected_calls' => $tests_per_level * 5,
    ];

    yield 'Log Level error' => [
      'min_level' => 'error',
      'expected_calls' => $tests_per_level * 4,
    ];

    yield 'Log Level critical' => [
      'min_level' => 'critical',
      'expected_calls' => $tests_per_level * 3,
    ];

    yield 'Log Level alert' => [
      'min_level' => 'alert',
      'expected_calls' => $tests_per_level * 2,
    ];

    yield 'Log Level emergency' => [
      'min_level' => 'emergency',
      'expected_calls' => $tests_per_level,
    ];

  }

  /**
   * Test logging an unknown log level.
   */
  public function testLogWithUnknownLevel(): void {
    $this->loggerMock
      ->expects($this->never())
      ->method('log');
    $logger = new VaultLogLevelFilter($this->loggerMock, 'debug');
    $this->expectException(InvalidArgumentException::class);
    $logger->log('unknown', 'Sample log message');
  }

}

/**
 * Test class for \Stringable text.
 */
class StringableText implements \Stringable {

  public function __construct(
    private readonly string $string,
  ) {}

  /**
   * Cast to string.
   *
   * @return string
   *   String representation of level.
   */
  public function __toString(): string {
    return "$this->string";
  }

}
