<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\vault\VaultConfig;

/**
 * Tests the Vault Client Factory.
 *
 * @group vault
 *
 * @covers \Drupal\vault\VaultConfig
 * @codeCoverageIgnore
 */
class VaultConfigUnitTest extends UnitTestCase {

  /**
   * Test the VaultConfig service.
   *
   * @dataProvider providerConfigTest
   */
  public function testVaultConfig(array $vault_config, string $method, mixed $expected_result): void {

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->getConfigFactoryStub(['vault.settings' => $vault_config]);

    $config_service = new VaultConfig($config_factory);
    $callable = [$config_service, $method];

    if (is_callable($callable)) {
      $result = call_user_func($callable);
    }
    else {
      $this->fail('Method is not callable');
    }

    $this->assertEquals($expected_result, $result, "Value matches expected");
  }

  /**
   * Provide data for testing the vault.config service.
   *
   * @return array
   *   Array of test parameters keyed by test name.
   *
   * @phpstan-return array<array{0: array, 1: string, 2: mixed}>
   */
  public function providerConfigTest(): array {
    return [
      'Get base Url empty' => [
        [],
        'getBaseUrl',
        'https://vault:8200',
      ],
      'Get base Url non-string' => [
        ['base_url' => ['data']],
        'getBaseUrl',
        'https://vault:8200',
      ],
      'Get base Url defined' => [
        ['base_url' => 'http://vault.localhost:8200'],
        'getBaseUrl',
        'http://vault.localhost:8200',
      ],
      'Get Auth Plugin Name undefined' => [
        [],
        'getAuthPluginName',
        NULL,
      ],
      'Get Auth Plugin Name non-string' => [
        ['plugin_auth' => ['token']],
        'getAuthPluginName',
        NULL,
      ],
      'Get Auth Plugin Name token' => [
        ['plugin_auth' => 'token'],
        'getAuthPluginName',
        'token',
      ],
      'Get Auth Plugin Name empty string' => [
        ['plugin_auth' => ''],
        'getAuthPluginName',
        NULL,
      ],
      'Get Auth Plugin Config undefined' => [
        [],
        'getAuthPluginConfig',
        [],
      ],
      'Get Auth Plugin Config not array' => [
        ['auth_plugin_config' => 'data'],
        'getAuthPluginConfig',
        [],
      ],
      'Get Auth Plugin Config valid array' => [
        ['auth_plugin_config' => ['key' => 'value']],
        'getAuthPluginConfig',
        ['key' => 'value'],
      ],
      'Get Cron Renew Enabled unset' => [
        [],
        'getCronRenewEnabled',
        FALSE,
      ],
      'Get Cron Renew Enabled TRUE' => [
        ['lease_renew_cron' => TRUE],
        'getCronRenewEnabled',
        TRUE,
      ],
      'Get Cron Renew Enabled FALSE' => [
        ['lease_renew_cron' => FALSE],
        'getCronRenewEnabled',
        FALSE,
      ],
      'Get Lease Plugin Name undefined' => [
        [],
        'getLeasePluginName',
        'state',
      ],
      'Get Lease Plugin Name non-string' => [
        ['plugin_lease_storage' => ['token']],
        'getLeasePluginName',
        'state',
      ],
      'Get Lease Plugin Name encrypted_state' => [
        ['plugin_lease_storage' => 'encrypted_state'],
        'getLeasePluginName',
        'encrypted_state',
      ],
      'Get Lease Plugin Name empty string' => [
        ['plugin_lease_storage' => ''],
        'getLeasePluginName',
        'state',
      ],
      'Get Lease Plugin Config undefined' => [
        [],
        'getLeasePluginConfig',
        [],
      ],
      'Get Lease Plugin Config not array' => [
        ['lease_storage_plugin_config' => 'data'],
        'getLeasePluginConfig',
        [],
      ],
      'Get Lease Plugin Config valid array' => [
        ['lease_storage_plugin_config' => ['key' => 'value']],
        'getLeasePluginConfig',
        ['key' => 'value'],
      ],
      'Get Lease TTL Increment unset' => [
        [],
        'getLeaseTtlIncrement',
        86400,
      ],
      'Get Lease TTL Increment non-int' => [
        ['lease_ttl_increment' => '3200'],
        'getLeaseTtlIncrement',
        86400,
      ],
      'Get Lease TTL Increment non-zero positive int' => [
        ['lease_ttl_increment' => 3200],
        'getLeaseTtlIncrement',
        3200,
      ],
      'Get Lease TTL Increment 0' => [
        ['lease_ttl_increment' => 0],
        'getLeaseTtlIncrement',
        86400,
      ],
      'Get Lease TTL Increment negative integer' => [
        ['lease_ttl_increment' => -3200],
        'getLeaseTtlIncrement',
        86400,
      ],
    ];
  }

}
