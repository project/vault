<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\vault\Unit\Mocks\TestPruneableCacheInterface;
use Drupal\vault\VaultCacheManager;
use Drupal\vault\VaultClientInterface;
use Drupal\vault\VaultConfigInterface;
use Psr\Cache\CacheItemPoolInterface;

include __DIR__ . '/../../../vault.module';

/**
 * Tests the VaultCacheManager custom functions.
 *
 * @group vault
 *
 * @codeCoverageIgnore
 */
class VaultModuleUnitTest extends UnitTestCase {

  /**
   * Ensure hook_cache_flush calls Vault Cache Manager.
   *
   * @covers \vault_cache_flush()
   */
  public function testHookCacheFlush(): void {

    $cache = $this->createMock(CacheItemPoolInterface::class);
    $cache
      ->expects($this->once())
      ->method('clear')
      ->willReturn(TRUE);
    $manager = new VaultCacheManager($cache);

    // Set mock services into global container.
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $container->set('vault.cache.manager', $manager);
    vault_cache_flush();
  }

  /**
   * Test hook_cron() calls for cache prune.
   *
   * @covers \vault_cache_flush()
   */
  public function testHookCronPruneCache(): void {

    $cache = $this->createMock(TestPruneableCacheInterface::class);
    $cache
      ->expects($this->once())
      ->method('prune')
      ->willReturn(TRUE);
    $manager = new VaultCacheManager($cache);

    $client_mock = $this->createMock(VaultClientInterface::class);
    $config_mock = $this->createMock(VaultConfigInterface::class);
    $config_mock->method('getCronRenewEnabled')->willReturn(FALSE);

    // Set mock services into global container.
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $container->set('vault.cache.manager', $manager);
    $container->set('vault.vault_client', $client_mock);
    $container->set('vault.config', $config_mock);
    vault_cron();
  }

}
