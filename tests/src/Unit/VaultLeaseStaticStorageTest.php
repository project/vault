<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Component\Datetime\Time;
use Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseStaticStorage;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the VaultLeaseStateStorage plugin.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseStaticStorage
 * @covers \Drupal\vault\Plugin\VaultLeaseStorageBase
 * @codeCoverageIgnore
 */
class VaultLeaseStaticStorageTest extends VaultLeaseStoragePluginBase {

  /**
   * Unix timestamp for getCurrentTime() mock response.
   *
   * @var int
   */
  protected int $currentTimeValue;

  /**
   * Setup the storage mock for use with the tests.
   */
  public function setUp(): void {
    parent::setUp();

    $logger_mock = $this->createMock(LoggerInterface::class);

    $this->currentTimeValue = 176000000;
    $time_mock = $this->createMock(Time::class);
    $time_mock->method('getCurrentTime')->willReturnReference($this->currentTimeValue);

    $this->plugin = new VaultLeaseStaticStorage([], 'state', [], $this->clientMock, $logger_mock, $time_mock);
    $this->plugin->setLease(self::VALID_LEASE_1_HASHED, $this->leaseDataLookup['valid_lease_1']['lease_id'], $this->leaseDataLookup['valid_lease_1']['data'], 86400, TRUE);
  }

  /**
   * Test create() method.
   */
  public function testCreate(): void {

    $client_mock = $this->createMock(VaultClientInterface::class);
    $logger_mock = $this->createMock(LoggerInterface::class);
    $time_mock = $this->createMock(Time::class);

    $service_map = [
      [
        'vault.vault_client_no_lease_storage',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $client_mock,
      ],
      [
        'logger.channel.vault',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $logger_mock,
      ],
      [
        'datetime.time',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $time_mock,
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = VaultLeaseStaticStorage::create($container_mock, [], 'state', []);
    $this->assertInstanceOf(VaultLeaseStaticStorage::class, $plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function testSetLease(): void {
    parent::testSetLease();
    $this->assertSame($this->plugin->getLease('valid_lease_1'), $this->leaseDataLookup['valid_lease_1']['data']);
  }

  /**
   * {@inheritdoc}
   */
  public function testUpdateLeaseExpires(): void {
    // Original lease expires at 176086400, test renew before expire.
    $this->currentTimeValue = 176086000;
    parent::testUpdateLeaseExpires();
    // Renewed lease should now expire at 176172400.
    $this->currentTimeValue = 176172399;
    $this->assertNotNull($this->plugin->getLease('valid_lease_1'));
    $this->currentTimeValue = 176172400;
    $this->assertNull($this->plugin->getLease('valid_lease_1'));
  }

}
