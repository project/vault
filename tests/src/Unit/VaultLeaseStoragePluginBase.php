<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Site\Settings;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\Plugin\VaultLeaseStorageInterface;
use Drupal\vault\VaultClientInterface;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use Vault\ResponseModels\Response;

/**
 * Tests the VaultClient custom functions.
 *
 * Those extending this class should provide their own create function.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Plugin\VaultLeaseStorageBase
 * @codeCoverageIgnore
 */
abstract class VaultLeaseStoragePluginBase extends UnitTestCase {

  /* cSpell:disable */
  /**
   * Salted+hashed value of 'nonexistent_lease'.
   */
  const NONEXISTENT_LEASE_HASHED = 'hashed::SDhvlPasj538elrKWw7jdcevy2GBnFakOilG6CtsNOg';

  /**
   * Salted+hashed value of 'valid_lease_1'.
   */
  const VALID_LEASE_1_HASHED = 'hashed::IuRRqIeZ13c-hXwuiCq-S2dpT-s5UGv9hI7tPz7NQ1U';
  /* cSpell:enable */

  /**
   * A mock VaultClient.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $clientMock;

  /**
   * A mapping of valid lease data.
   *
   * This can be used for any mocks that require access to test variables.
   *
   * @var array
   */
  protected array $leaseDataLookup = [
    'valid_lease_1' => [
      'lease_id' => 'rabbitmq1',
      'renewable' => TRUE,
      'data' => [
        'username' => 'username',
        'password' => 'password',
      ],
    ],
    'not_renewable_lease' => [
      'lease_id' => 'rabbitmq1-not-renewable',
      'renewable' => FALSE,
      'data' => [
        'username' => 'username',
        'password' => 'password',
      ],
    ],
  ];

  /**
   * The plugin under test.
   *
   * @var \Drupal\vault\Plugin\VaultLeaseStorageInterface
   */
  protected VaultLeaseStorageInterface $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    /* cspell:disable-next-line */
    $site_settings['hash_salt'] = 'fuyfZ5BcN3jn8R18sQHDtrsr-XyF-a1-R4R0e3tXmXnEz2I88feSDWjjQvlLFKMvoCAaNKEURg';
    new Settings($site_settings);

    $this->clientMock = $this->createMock(VaultClientInterface::class);
    $this->clientMock->method('buildPath')->willReturnCallback(
      function ($path) {
        return sprintf('/%s%s', 'v1', $path);
      }
    );
  }

  /**
   * Test the getLease() function.
   *
   * @dataProvider providerGetLease
   */
  public function testGetLease(string $storage_key, ?array $expected): void {
    $result = $this->plugin->getLease($storage_key);
    if ($expected == NULL) {
      $this->assertNull($result);
      return;
    }

    $this->assertEquals($expected['data'], $result);

  }

  /**
   * Data provider for testGetLease().
   *
   * @return array[]
   *   Array of variables to test with.
   */
  public function providerGetLease(): array {

    return [
      'Invalid Lease' => [
        'invalid_lease_1',
        NULL,
      ],
      'Valid Lease' => [
        'valid_lease_1',
        $this->leaseDataLookup['valid_lease_1'],
      ],
      'Hashed lease key forbidden' => [
        self::VALID_LEASE_1_HASHED,
        NULL,
      ],
    ];
  }

  /**
   * Test setting a lease.
   *
   * The child of this class should ensure an assertion based on the data below.
   */
  public function testSetLease(): void {
    $this->plugin->setLease('valid_lease_1', $this->leaseDataLookup['valid_lease_1']['lease_id'], $this->leaseDataLookup['valid_lease_1']['data'], 86400, TRUE);
    $this->plugin->setLease(self::VALID_LEASE_1_HASHED, $this->leaseDataLookup['valid_lease_1']['lease_id'], $this->leaseDataLookup['valid_lease_1']['data'], 86400, TRUE);
  }

  /**
   * Test Updating lease expiration.
   *
   * The child of this class should ensure an assertion based on the data below.
   */
  public function testUpdateLeaseExpires(): void {
    $this->assertTrue($this->plugin->updateLeaseExpires('valid_lease_1', 86400, TRUE));
    $this->assertFalse($this->plugin->updateLeaseExpires('lease_does_not_exist', 86400, TRUE));
  }

  /**
   * Test obtaining plugin current configuration.
   */
  public function testGetConfiguration(): void {
    if (!$this->plugin instanceof ConfigurableInterface) {
      $this->markTestSkipped("Plugin does not utilize ConfigurableInterface");
    }
    $this->assertIsArray($this->plugin->getConfiguration());
  }

  /**
   * Obtain default configuration from plugin.
   */
  public function testDefaultConfiguration(): void {
    if (!$this->plugin instanceof ConfigurableInterface) {
      $this->markTestSkipped("Plugin does not utilize ConfigurableInterface");
    }
    $this->assertIsArray($this->plugin->defaultConfiguration());
  }

  /**
   * Test setting configuration on the plugin.
   *
   * @doesNotPerformAssertions
   */
  public function testSetConfiguration(): void {
    if (!$this->plugin instanceof ConfigurableInterface) {
      $this->markTestSkipped("Plugin does not utilize ConfigurableInterface");
    }
    $this->plugin->setConfiguration($this->plugin->defaultConfiguration());
  }

  /**
   * Test revokeLease()
   *
   * @dataProvider providerRevokeLease
   */
  public function testRevokeLease(string $lease_name, InvocationOrder $revoke_put_invoker, bool $expected, \Exception|Response $response): void {

    $this->clientMock
      ->expects($revoke_put_invoker)
      ->method('put')
      ->with(
        $this->equalTo('/v1/sys/leases/revoke'),
        $this->isType('string')
      )
      ->willReturnCallback(function (string $param) use ($response) {
        if (is_a($response, \Exception::class)) {
          throw $response;
        }
        return $response;
      });

    $this->assertSame($expected, $this->plugin->revokeLease($lease_name));
  }

  /**
   * Data provider for testRevokeLease().
   */
  public function providerRevokeLease(): \Generator {
    yield 'non-existent lease' => [
      'lease_name' => 'nonexistent_lease',
      'revoke_put_invoker' => self::never(),
      'expected' => FALSE,
      'response' => new \Exception('Test Error: Attempted to revoke non-existent lease'),
    ];

    yield 'Success valid lease' => [
      'lease_name' => 'valid_lease_1',
      'revoke_put_invoker' => self::once(),
      'expected' => TRUE,
      'response' => new Response(),
    ];

    yield 'Error during revoke valid lease' => [
      'lease_name' => 'valid_lease_1',
      'revoke_put_invoker' => self::once(),
      'expected' => TRUE,
      'response' => new Response(),
    ];

    yield 'Exception during revoke' => [
      'lease_name' => 'valid_lease_1',
      'revoke_put_invoker' => self::once(),
      'expected' => FALSE,
      'response' => new \Exception('fake exception'),
    ];

  }

  /**
   * Tests renewLease()
   *
   * @dataProvider providerRenewLease
   */
  public function testRenewLease(string $lease_name, InvocationOrder $renew_put_invoker, bool $expected, \Exception|Response $response): void {

    $this->clientMock
      ->expects($renew_put_invoker)
      ->method('put')
      ->with(
        $this->equalTo('/v1/sys/leases/renew'),
        $this->isType('string')
      )
      ->willReturnCallback(function (string $param) use ($response) {
        if (is_a($response, \Exception::class)) {
          throw $response;
        }
        return $response;
      });

    $this->assertSame($expected, $this->plugin->renewLease($lease_name, 200));
  }

  /**
   * Data provider for testRenewLease().
   */
  public function providerRenewLease(): \Generator {
    $valid_lease_response = [
      "requestId" => "852a30ee-39cc-ba83-d9fe-d475fa82adf7",
      /* cspell:disable-next-line */
      "leaseId" => "rabbitmq/creds/my-role/RlQVpSpKCRyfMkXMTGFi6bvX",
      "renewable" => TRUE,
      "leaseDuration" => 86400,
      /* cspell:disable-next-line */
      "data" => 'password":"cyqCLRUvawG1rKgkgj14w7Pi3uc5ZwOT49ik","username":"token-35342576-5df4-1dcc-2ebb-8c524c04e4c5"',
      "auth" => NULL,
    ];

    yield 'non-existent lease' => [
      'lease_name' => 'nonexistent_lease',
      'renew_put_invoker' => self::never(),
      'expected' => FALSE,
      'response' => new \Exception('Test Error: Attempted to renew non-existent lease'),
    ];

    yield 'Success valid lease' => [
      'lease_name' => 'valid_lease_1',
      'renew_put_invoker' => self::once(),
      'expected' => TRUE,
      'response' => new Response($valid_lease_response),
    ];

    yield 'Exception during revoke' => [
      'lease_name' => 'valid_lease_1',
      'renew_put_invoker' => self::once(),
      'expected' => FALSE,
      'response' => new \Exception('fake exception'),
    ];

    yield 'Existing lease is not renewable' => [
      'lease_name' => 'not_renewable_lease',
      'renew_put_invoker' => self::never(),
      'expected' => FALSE,
      'response' => new \Exception('fake exception'),
    ];

  }

  /**
   * Tests renewAllLeases()
   *
   * @dataProvider providerRenewAllLeases
   */
  public function testRenewAllLeases(\Exception|Response $response): void {
    $this->clientMock
      ->expects($this->atLeastOnce())
      ->method('put')
      ->willReturnCallback(function (string $param) use ($response) {
        if (is_a($response, \Exception::class)) {
          throw $response;
        }
        return $response;
      });

    $this->plugin->renewAllLeases(86400);
  }

  /**
   * Data provider for testRenewAlLeases.
   */
  public function providerRenewAllLeases(): \Generator {
    $valid_lease_response = [
      "requestId" => "852a30ee-39cc-ba83-d9fe-d475fa82adf7",
      /* cspell:disable-next-line */
      "leaseId" => "rabbitmq/creds/my-role/RlQVpSpKCRyfMkXMTGFi6bvX",
      "renewable" => TRUE,
      "leaseDuration" => 86400,
      /* cspell:disable-next-line */
      "data" => 'password":"cyqCLRUvawG1rKgkgj14w7Pi3uc5ZwOT49ik","username":"token-35342576-5df4-1dcc-2ebb-8c524c04e4c5"',
      "auth" => NULL,
    ];

    $no_longer_renewable_response = $valid_lease_response;
    $no_longer_renewable_response['renewable'] = FALSE;

    yield 'Exception occurred ' => [
      'response' => new \Exception('Test Exception'),
    ];

    yield 'Success valid lease' => [
      'response' => new Response($valid_lease_response),
    ];

    yield 'Success valid lease, no longer renewable' => [
      'response' => new Response($no_longer_renewable_response),
    ];
  }

}
