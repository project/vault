<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\vault\Plugin\VaultLeaseStorageInterface;
use Drupal\vault\VaultClient;
use Drupal\vault\VaultClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use Psr\Log\LoggerInterface;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;

/**
 * Tests the VaultClient custom functions.
 *
 * @group vault
 *
 * @coversDefaultClass \Drupal\vault\VaultClient
 * @codeCoverageIgnore
 */
class VaultClientUnitTest extends UnitTestCase {

  /**
   * Test listSecretEngineMounts.
   *
   * @dataProvider secretEnginesMountsProvider
   *
   * @covers ::listSecretEngineMounts
   * @covers ::listMounts
   */
  public function testListSecretEnginesMounts(array $key_types, array $expected_keys): void {
    $json_response = file_get_contents(__DIR__ . '/../../fixtures/Responses/sysMounts.json');
    $this->assertIsString($json_response);
    $mock = new MockHandler([
      // Authentication Check.
      new Response(200, []),
      // Engine response.
      new Response(202, ['Content-Length' => '0'], $json_response),
    ]);

    $client = $this->getClientWithMockResponse($mock);

    $results = $client->listSecretEngineMounts($key_types);

    foreach ($expected_keys as $expected_key) {
      $this->assertArrayHasKey($expected_key, $results);
    }

    if (count($expected_keys) == 0) {
      $this->assertEmpty($results, 'No results should be returned');
    }
  }

  /**
   * Provide data for Secret Engines Mount Test.
   *
   * @return array
   *   Test parameters.
   */
  public function secretEnginesMountsProvider(): array {
    return [
      'Empty None' => [
        [],
        [],
      ],
      'Empty No matching' => [
        ['invalid_mount_type'],
        [],
      ],
      'KV' => [
        ['kv'],
        ['secret/'],
      ],
      'cubbyhole' => [
        ['cubbyhole'],
        ['cubbyhole/'],
      ],
    ];
  }

  /**
   * Test storing lease data.
   *
   * @covers ::storeLease
   * @covers ::setLeaseStorage
   */
  public function testStoreLease(): void {
    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $client = $this->getClientWithMockResponse($mock_response);

    // Ensure that no exception occurs when lease storage not set.
    $client->storeLease('test_lease_1', '000000000000000000', [], 200, TRUE);

    $lease_storage_mock = $this->createMock(VaultLeaseStorageInterface::class);

    $lease_storage_mock->expects($this->once())
      ->method('setLease')
      ->with('test_lease_1', '000000000000000000', [], 200, TRUE);

    $client->setLeaseStorage($lease_storage_mock);
    $client->storeLease('test_lease_1', '000000000000000000', [], 200, TRUE);
  }

  /**
   * @covers ::retrieveLease
   */
  public function testRetrieveLease(): void {
    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $client = $this->getClientWithMockResponse($mock_response);

    // Always false when no lease storage set.
    $this->assertNull($client->retrieveLease('test_lease_1'), 'retrieveLease always NULL when no storage set');

    $lease_storage_mock = $this->createMock(VaultLeaseStorageInterface::class);

    $lease_storage_mock->expects($this->exactly(2))
      ->method('getLease')
      ->with($this->logicalOr('test_lease_1', 'test_lease_2'))
      ->willReturnOnConsecutiveCalls(['test' => TRUE], NULL);

    $client->setLeaseStorage($lease_storage_mock);
    $this->assertIsArray($client->retrieveLease('test_lease_1'));
    $this->assertNull($client->retrieveLease('test_lease_2'));
  }

  /**
   * @covers ::revokeLease
   */
  public function testRevokeLease(): void {
    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $client = $this->getClientWithMockResponse($mock_response);

    // Always false when no lease storage set.
    $this->assertFalse($client->revokeLease('valid_lease_1'), 'revokeLease always FALSE when no storage set');

    $lease_storage_mock = $this->createMock(VaultLeaseStorageInterface::class);

    $lease_storage_mock->expects($this->exactly(2))
      ->method('revokeLease')
      ->willReturnMap([
        ['mock_error_occurred', FALSE],
        ['valid_lease_1', TRUE],
      ]);

    $client->setLeaseStorage($lease_storage_mock);
    $this->assertFalse($client->revokeLease('mock_error_occurred'));
    $this->assertTrue($client->revokeLease('valid_lease_1'));
  }

  /**
   * @covers ::renewLease
   */
  public function testRenewLease(): void {
    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $client = $this->getClientWithMockResponse($mock_response);

    // Always false when no lease storage set.
    $this->assertFalse($client->renewLease('valid_lease_1', 300), 'renewLease should always return false with no lease storage set');

    $lease_storage_mock = $this->createMock(VaultLeaseStorageInterface::class);

    $lease_storage_mock->expects($this->exactly(2))
      ->method('renewLease')
      ->with(
        $this->logicalOr(
          $this->identicalTo('nonexistent_lease'),
          $this->identicalTo('valid_lease_1')
        ),
        200
      )
      ->willReturnMap([
        ['nonexistent_lease', 200, FALSE],
        ['valid_lease_1', 200, TRUE],
      ]);

    $client->setLeaseStorage($lease_storage_mock);
    $this->assertFalse($client->renewLease('nonexistent_lease', 200));
    $this->assertTrue($client->renewLease('valid_lease_1', 200));
  }

  /**
   * @covers ::renewAllLeases
   */
  public function testRenewAllLeases(): void {

    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $client = $this->getClientWithMockResponse($mock_response);

    // No error occurs when no lease storage set.
    $client->renewAllLeases(300);

    $lease_storage_mock = $this->createMock(VaultLeaseStorageInterface::class);

    $lease_storage_mock->expects($this->exactly(1))
      ->method('renewAllLeases')
      ->with(86400);

    $client->setLeaseStorage($lease_storage_mock);
    $client->renewAllLeases(86400);
  }

  /**
   * Provides a VaultClient that utilizes mocked responses.
   *
   * Lease storage must be mocked separately on the returned client.
   *
   * @param \GuzzleHttp\Handler\MockHandler $mock_response
   *   Mocked response to be injected into the http client handler.
   *
   * @return \Drupal\vault\VaultClientInterface
   *   The mocked vault client.
   */
  protected function getClientWithMockResponse(MockHandler $mock_response): VaultClientInterface {
    $base_uri = new Uri('http://vault:8200');

    $handlerStack = HandlerStack::create($mock_response);
    $guzzle_client = new Client(['handler' => $handlerStack]);
    $request_factory = new HttpFactory();
    $stream_factory = new HttpFactory();

    $logger_mock = $this->createMock(LoggerInterface::class);

    $client = new VaultClient($base_uri, $guzzle_client, $request_factory, $stream_factory, $logger_mock);

    $auth_strategy = new TokenAuthenticationStrategy('myroot');
    $client->setAuthenticationStrategy($auth_strategy)->authenticate();

    return $client;

  }

}
