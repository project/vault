<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseStateStorage;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the VaultLeaseStateStorage plugin.
 *
 * @group vault
 *
 * @covers \Drupal\vault\Plugin\VaultLeaseStorage\VaultLeaseStateStorage
 * @covers \Drupal\vault\Plugin\VaultLeaseStorageBase
 * @codeCoverageIgnore
 */
class VaultLeaseStateStorageTest extends VaultLeaseStoragePluginBase {

  /**
   * Mock of the K/V Storage that supports expiration time.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stateMock;

  /**
   * Setup the storage mock for use with the tests.
   */
  public function setUp(): void {
    parent::setUp();

    $storage_get_map = [
      // nonexistent_lease.
      [self::NONEXISTENT_LEASE_HASHED, NULL, NULL],
      // valid_lease_1.
      [
        self::VALID_LEASE_1_HASHED,
        NULL,
        $this->leaseDataLookup['valid_lease_1'],
      ],
    ];

    $this->stateMock = $this->createMock(KeyValueStoreExpirableInterface::class);

    $this->stateMock->expects($this->any())
      ->method('get')
      ->willReturnMap($storage_get_map);
    $this->stateMock->method('getAll')
      ->willReturn([self::VALID_LEASE_1_HASHED => $this->leaseDataLookup['valid_lease_1']]);

    $logger_mock = $this->createMock(LoggerInterface::class);

    $this->plugin = new VaultLeaseStateStorage([], 'state', [], $this->clientMock, $logger_mock, $this->stateMock);
  }

  /**
   * Test create() method.
   */
  public function testCreate(): void {

    $kv_factory_mock = $this->createMock(KeyValueFactoryInterface::class);
    $kv_factory_mock->method('get')
      ->willReturn($this->stateMock);

    $client_mock = $this->createMock(VaultClientInterface::class);
    $logger_mock = $this->createMock(LoggerInterface::class);

    $service_map = [
      [
        'vault.vault_client_no_lease_storage',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $client_mock,
      ],
      [
        'logger.channel.vault',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $logger_mock,
      ],
      [
        'keyvalue.expirable',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $kv_factory_mock,
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = VaultLeaseStateStorage::create($container_mock, [], 'state', []);
    $this->assertInstanceOf(VaultLeaseStateStorage::class, $plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function testSetLease(): void {

    $this->stateMock->expects($this->exactly(2))
      ->method('setWithExpire')
      ->with(self::VALID_LEASE_1_HASHED, $this->leaseDataLookup['valid_lease_1'], 86400);

    parent::testSetLease();
  }

  /**
   * {@inheritdoc}
   */
  public function testUpdateLeaseExpires(): void {

    $this->stateMock->expects($this->exactly(1))
      ->method('setWithExpire')
      ->with(self::VALID_LEASE_1_HASHED, $this->leaseDataLookup['valid_lease_1'], 86400);

    parent::testUpdateLeaseExpires();
  }

}
