<?php

namespace Drupal\Tests\vault\Unit;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\Plugin\VaultAuthManagerInterface;
use Drupal\vault\Plugin\VaultLeaseStorageManagerInterface;
use Drupal\vault\VaultClient;
use Drupal\vault\VaultClientFactory;
use Drupal\vault\VaultClientInterface;
use Drupal\vault\VaultConfigInterface;
use Drupal\vault_ci_auth\Plugin\VaultAuth\CIAuth;
use Drupal\vault_ci_state\Plugin\VaultLeaseStorage\CILease;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

/**
 * Tests the Vault Client Factory.
 *
 * @group vault
 *
 * @covers \Drupal\vault\VaultClientFactory
 * @codeCoverageIgnore
 */
class VaultClientFactoryUnitTest extends UnitTestCase {

  /**
   * Base vault settings.
   *
   * @var array
   */
  protected array $vaultSettings;

  /**
   * Logger service mock.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $loggerMock;

  /**
   * Vault Auth Plugin Manager mock.
   *
   * @var \Drupal\vault\Plugin\VaultAuthManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $authManagerMock;

  /**
   * Vault Lease State Storage Plugin Manager Mock.
   *
   * @var \Drupal\vault\Plugin\VaultLeaseStorageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $leaseStorageMock;

  /**
   * Setup test data.
   */
  public function setUp(): void {
    parent::setUp();

    $this->vaultSettings = [
      'getBaseUrl' => 'http://vault:8200',
      'getAuthPluginName' => 'ciauth',
      'getAuthPluginConfig' => [],
      'getLeasePluginName' => 'state',
      'getLeasePluginConfig' => [],
    ];
    $this->loggerMock = $this->createMock(LoggerInterface::class);

    $this->authManagerMock = $this->createMock(VaultAuthManagerInterface::class);
    $this->authManagerMock->method('createInstance')
      ->willReturnCallback([$this, 'authPluginCallback']);

    $this->leaseStorageMock = $this->createMock(VaultLeaseStorageManagerInterface::class);
    $this->leaseStorageMock->method('createInstance')
      ->willReturnCallback([$this, 'leasePluginCallback']);

  }

  /**
   * Test the client factory.
   *
   * @dataProvider providerClientFactory
   */
  public function testVaultClientFactory(array $vault_config, bool $set_state, ?string $expect_exception = ''): void {

    $vault_config += $this->vaultSettings;
    $vault_config_mock = $this->createMock(VaultConfigInterface::class);
    foreach ($vault_config as $method => $value) {
      $vault_config_mock->method($method)->willReturn($value);
    }

    $mock_response = new MockHandler([
      // Authentication Check.
      new Response(200, []),
    ]);
    $handlerStack = HandlerStack::create($mock_response);
    $guzzle_client = new Client(['handler' => $handlerStack]);

    if (!empty($expect_exception)) {
      $this->expectExceptionMessage($expect_exception);
    }

    $factory_response = VaultClientFactory::createInstance($vault_config_mock, $this->loggerMock, $this->authManagerMock, $this->leaseStorageMock, $guzzle_client, new HttpFactory(), new HttpFactory(), new ArrayAdapter(), $set_state);

    $this->assertInstanceOf(VaultClient::class, $factory_response);
  }

  /**
   * Provide data for testing the VaultClientFactory.
   *
   * @return array
   *   Array of test parameters keyed by test name.
   */
  public function providerClientFactory(): array {
    return [
      'Base level test' => [
        [],
        FALSE,
        NULL,
      ],
      'Non-Existent Auth token' => [
        ['getAuthPluginName' => 'invalid_auth'],
        FALSE,
        'auth plugin not found',
      ],
      'Auth Plugin: Empty plugin name' => [
        ['getAuthPluginName' => NULL],
        FALSE,
        'No auth plugin configured',
      ],
      'Lease plugin: CILease' => [
        [
          'getLeasePluginName' => 'cilease',
        ],
        TRUE,
        NULL,
      ],
    ];
  }

  /**
   * Provides response for mock VaultAuthPluginManager::CreateInstance()
   *
   * @param string $plugin_name
   *   Ignored.
   * @param array $plugin_config
   *   Ignored.
   *
   * @return \Drupal\vault_ci_auth\Plugin\VaultAuth\CIAuth
   *   A vault auth plugin intended for CI labs only.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Simulated exception for when plugin does not exist.
   */
  public function authPluginCallback(string $plugin_name, array $plugin_config): CIAuth {
    if ($plugin_name !== 'ciauth') {
      throw new PluginNotFoundException('auth plugin not found');
    }

    return new CIAuth([], 'ciauth', []);
  }

  /**
   * Provides response for mock VaultLeaseStorageManager::CreateInstance()
   *
   * @param string $plugin_name
   *   Ignored.
   * @param array $plugin_config
   *   Ignored.
   *
   * @return \Drupal\vault_ci_state\Plugin\VaultLeaseStorage\CILease
   *   A vault lease plugin intended for CI labs only.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Simulated exception for when plugin does not exist.
   */
  public function leasePluginCallback(string $plugin_name, array $plugin_config): CILease {
    if ($plugin_name !== 'cilease' && $plugin_name !== 'state') {
      throw new PluginNotFoundException('lease plugin not found');
    }

    $client_mock = $this->createMock(VaultClientInterface::class);
    $logger_mock = $this->createMock(LoggerInterface::class);

    return new CILease([], 'cilease', [], $client_mock, $logger_mock);
  }

}
