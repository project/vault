<?php

namespace Drupal\Tests\vault\Unit\Mocks;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\PruneableInterface;

/**
 * Interface for mocking a pruneable cache.
 *
 * PHPUnit 9 does not support createMockForIntersectionOfInterfaces()
 * requiring us to create a simulated interface for createMock().
 */
interface TestPruneableCacheInterface extends CacheItemPoolInterface, PruneableInterface {
}
