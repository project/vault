<?php

namespace Drupal\vault\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Vault Authentication item annotation object.
 *
 * @see \Drupal\vault\Plugin\VaultAuthManager
 * @see plugin_api
 *
 * @Annotation
 */
final class VaultAuth extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

}
