<?php

namespace Drupal\vault;

use Drupal\vault\Plugin\VaultLeaseStorageInterface;
use Vault\CachedClient;

/**
 * Wrapper for \Vault\Client providing some helper methods.
 *
 * It also acts as a translation layer between vault leases and drupal entities
 * implementing leases (like keys).
 */
final class VaultClient extends CachedClient implements VaultClientInterface {

  public const API = 'v1';

  /**
   * The lease storage object.
   *
   * @var \Drupal\vault\Plugin\VaultLeaseStorageInterface|null
   */
  protected ?VaultLeaseStorageInterface $leaseStorage = NULL;

  /**
   * {@inheritdoc}
   */
  public function setLeaseStorage(VaultLeaseStorageInterface $leaseStorage): void {
    $this->leaseStorage = $leaseStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function listMounts(): ?array {
    return $this->read('/sys/mounts')->getData();
  }

  /**
   * {@inheritdoc}
   */
  public function listSecretEngineMounts(array $engine_types): array {
    $data = $this->listMounts();
    if ($data == NULL) {
      return [];
    }
    return array_filter($data, function ($v) use ($engine_types) {
      return in_array($v['type'], $engine_types);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function storeLease(string $storage_key, string $lease_id, $data, int $expires, bool $renewable): void {
    if ($this->leaseStorage == NULL) {
      return;
    }
    $this->leaseStorage->setLease($storage_key, $lease_id, $data, $expires, $renewable);
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveLease(string $storage_key): mixed {
    return $this->leaseStorage?->getLease($storage_key);
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLease(string $storage_key): bool {
    if ($this->leaseStorage == NULL) {
      return FALSE;
    }
    return $this->leaseStorage->revokeLease($storage_key);
  }

  /**
   * {@inheritdoc}
   */
  public function renewLease(string $storage_key, int $increment): bool {
    if ($this->leaseStorage == NULL) {
      return FALSE;
    }
    return $this->leaseStorage->renewLease($storage_key, $increment);
  }

  /**
   * {@inheritdoc}
   */
  public function renewAllLeases(int $increment): void {
    if ($this->leaseStorage == NULL) {
      return;
    }
    $this->leaseStorage->renewAllLeases($increment);
  }

}
