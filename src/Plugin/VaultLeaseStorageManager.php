<?php

namespace Drupal\vault\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Vault Lease Storage plugin manager.
 */
final class VaultLeaseStorageManager extends DefaultPluginManager implements VaultLeaseStorageManagerInterface {

  /**
   * Constructs a new VaultLeaseStorageManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VaultLeaseStorage', $namespaces, $module_handler, 'Drupal\vault\Plugin\VaultLeaseStorageInterface', 'Drupal\vault\Annotation\VaultLeaseStorage');

    $this->alterInfo('vault_vault_lease_storage_info');
    $this->setCacheBackend($cache_backend, 'vault_vault_lease_storage_plugins');
  }

}
