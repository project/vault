<?php

namespace Drupal\vault\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Empty interface for use with mocking the Auth Manager service.
 *
 * @internal
 */
interface VaultAuthManagerInterface extends PluginManagerInterface {

}
