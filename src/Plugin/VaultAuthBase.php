<?php

namespace Drupal\vault\Plugin;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Vault Authentication plugins.
 *
 * @api
 */
abstract class VaultAuthBase extends PluginBase implements VaultAuthInterface, ContainerFactoryPluginInterface, DependentPluginInterface {

  /**
   * {@inheritdoc}
   */
  abstract public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self;

}
