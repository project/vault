<?php

namespace Drupal\vault\Plugin\VaultLeaseStorage;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\encrypt\Exception\EncryptException;
use Drupal\encrypt\Exception\EncryptionMethodCanNotDecryptException;
use Drupal\vault\Plugin\VaultLeaseStorageBase;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a token-based authentication strategy for the vault client.
 *
 * @VaultLeaseStorage(
 *   id = "encrypted_state",
 *   label = "Encrypted Key/Value",
 *   description = @Translation("Encrypts dynamic leases prior to storing in Drupal's Expirable Key/Value service"),
 *   provider = "encrypt"
 * )
 */
final class VaultLeaseEncryptedStateStorage extends VaultLeaseStorageBase implements PluginFormInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * The storage handler.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $storage;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityManager;

  /**
   * ID of EncryptionProfile to be used for encryption/decryption of data.
   *
   * @var string|null
   */
  protected ?string $encryptionProfileId;

  /**
   * Encrypt module service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface
   */
  protected EncryptServiceInterface $encryptService;

  /**
   * VaultLeaseEncryptedStateStorage constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface|null $client
   *   A non-lease-caching vault client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   String Translation service.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $storage
   *   Key/Value storage that supports expire times.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\encrypt\EncryptServiceInterface $encrypt_service
   *   Encryption service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ?VaultClientInterface $client, LoggerInterface $logger, TranslationInterface $translation, KeyValueStoreExpirableInterface $storage, EntityTypeManagerInterface $entity_type_manager, EncryptServiceInterface $encrypt_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $client, $logger);

    $this->storage = $storage;
    $this->entityManager = $entity_type_manager;
    $this->setConfiguration($configuration);
    $this->encryptionProfileId = $configuration['encryption_profile'] ?? NULL;
    $this->encryptService = $encrypt_service;
    $this->setStringTranslation($translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vault.vault_client_no_lease_storage'),
      $container->get('logger.channel.vault'),
      $container->get('string_translation'),
      $container->get('keyvalue.expirable')->get('vault'),
      $container->get('entity_type.manager'),
      $container->get('encryption')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeaseRaw(string $storage_key): ?array {
    // During renewAllLeases we will only know the hashed storage_key.
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $encrypted_data = $this->storage->get($storage_key);
    if ($encrypted_data == NULL || !is_string($encrypted_data)) {
      return NULL;
    }

    if ($this->encryptionProfileId == NULL) {
      return NULL;
    }
    /** @var \Drupal\encrypt\EncryptionProfileInterface $encryption_profile */
    $encryption_profile = $this->entityManager
      ->getStorage('encryption_profile')
      ->load($this->encryptionProfileId);

    try {
      $serialized_data = $this->encryptService->decrypt($encrypted_data, $encryption_profile);
    }
    catch (EncryptException | EncryptionMethodCanNotDecryptException $e) {
      return NULL;
    }

    $stored_lease = json_decode($serialized_data, TRUE);
    if (
      empty($stored_lease)
      || !is_array($stored_lease)
      || empty($stored_lease['lease_id'])
      || empty($stored_lease['data'])
      || empty($stored_lease['renewable'])
    ) {
      return NULL;
    }

    return $stored_lease;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllLeases(): array {
    $items = $this->storage->getAll();
    $returned = [];

    // If we don't have an encryption profile set return none.
    if ($this->encryptionProfileId == NULL) {
      return $returned;
    }

    foreach ($items as $key => $encrypted_data) {
      if ($encrypted_data == NULL) {
        continue;
      }

      /** @var \Drupal\encrypt\EncryptionProfileInterface $encryption_profile */
      $encryption_profile = $this->entityManager
        ->getStorage('encryption_profile')
        ->load($this->encryptionProfileId);

      try {
        $serialized_data = $this->encryptService->decrypt($encrypted_data, $encryption_profile);
      }
      catch (EncryptException | EncryptionMethodCanNotDecryptException $e) {
        continue;
      }
      $item = json_decode($serialized_data, TRUE);

      if (
        empty($item)
        || !is_array($item)
        || empty($item['lease_id'])
        || empty($item['data'])
      ) {
        continue;
      }
      $returned[$key] = $item['data'];
    }
    return $returned;
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteLease(string $storage_key): void {
    $this->storage->delete($storage_key);
  }

  /**
   * {@inheritdoc}
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void {
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $payload = [
      'lease_id' => $lease_id,
      'renewable' => $renewable,
      'data' => $data,
    ];
    $serialized_payload = json_encode($payload);
    if ($serialized_payload == FALSE) {
      throw new \Exception('failed to encode data in setLease');
    }

    if ($this->encryptionProfileId == NULL) {
      return;
    }

    /** @var \Drupal\encrypt\EncryptionProfileInterface $encryption_profile */
    $encryption_profile = $this->entityManager
      ->getStorage('encryption_profile')
      ->load($this->encryptionProfileId);

    try {
      $encrypted_data = $this->encryptService->encrypt($serialized_payload, $encryption_profile);
    }
    catch (EncryptException $e) {
      // The client will obtain a new lease as this one could not be stored.
      return;
    }
    $this->storage->setWithExpire($storage_key, $encrypted_data, $expires);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    // @todo Implement buildConfigurationForm() method.
    $available_profiles = [];
    $encryption_storage = $this->entityManager->getStorage('encryption_profile');
    /** @var \Drupal\encrypt\EncryptionProfileInterface $profile */
    foreach ($encryption_storage->loadMultiple() as $profile) {
      $available_profiles[$profile->id()] = $profile->label();
    }
    $form['encryption_profile'] = [
      '#type' => 'select',
      '#title' => $this->t('Encryption Profile'),
      '#options' => $available_profiles,
      '#default_value' => $this->encryptionProfileId,
      '#description' => $this->t(
        'The encryption profile that will be utilized for securing the data. If you do not see the profile you may <a href=":link">create a new profile</a>',
        [':link' => Url::fromRoute('entity.encryption_profile.add_form')->toString()]
      ),

    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // No additional validation required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
    $this->encryptionProfileId = $configuration['encryption_profile'] ?? NULL;
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [
      'encryption_profile' => NULL,
    ];
  }

}
