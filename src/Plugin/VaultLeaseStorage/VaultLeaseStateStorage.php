<?php

namespace Drupal\vault\Plugin\VaultLeaseStorage;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Site\Settings;
use Drupal\vault\Plugin\VaultLeaseStorageBase;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a token-based authentication strategy for the vault client.
 *
 * @VaultLeaseStorage(
 *   id = "state",
 *   label = "Cleartext Key/Value",
 *   description = @Translation("Cleartext storage of dynamic leases in Drupal's Expirable Key/Value service"),
 * )
 */
final class VaultLeaseStateStorage extends VaultLeaseStorageBase implements PluginFormInterface {

  /**
   * The storage handler.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $storage;

  /**
   * VaultLeaseStateStorage constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface|null $client
   *   A non-lease-caching vault client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $storage
   *   Key/Value storage that supports expire times.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ?VaultClientInterface $client, LoggerInterface $logger, KeyValueStoreExpirableInterface $storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $client, $logger);
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    // During initial setup or if credentials are invalid we do not have a
    // client available.
    try {
      $vault_client = $container->get('vault.vault_client_no_lease_storage');
    }
    catch (\Exception $e) {
      $vault_client = NULL;
    }

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $vault_client,
      $container->get('logger.channel.vault'),
      $container->get('keyvalue.expirable')->get('vault_lease')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeaseRaw(string $storage_key): ?array {
    // During renewAllLeases we will only know the hashed storage_key.
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $stored_lease = $this->storage->get($storage_key);
    if (
      empty($stored_lease)
      || !is_array($stored_lease)
      || empty($stored_lease['lease_id'])
      || empty($stored_lease['data'])
      || empty($stored_lease['renewable'])
    ) {
      return NULL;
    }

    return $stored_lease;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllLeases(): array {
    $items = $this->storage->getAll();
    $returned = [];
    foreach ($items as $key => $item) {
      $returned[$key] = $item['data'];
    }
    return $returned;
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteLease(string $storage_key): void {
    $this->storage->delete($storage_key);
  }

  /**
   * {@inheritdoc}
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void {
    // During renewAllLeases we will only know the hashed storage_key.
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $payload = [
      'lease_id' => $lease_id,
      'renewable' => $renewable,
      'data' => $data,
    ];
    $this->storage->setWithExpire($storage_key, $payload, $expires);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['warning'] = [
      '#theme' => 'cleartext-lease-storage-warning',
      '#type' => 'item',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // NOOP.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // NOOP.
  }

}
