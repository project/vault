<?php

namespace Drupal\vault\Plugin\VaultLeaseStorage;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\vault\Plugin\VaultLeaseStorageBase;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a static memory based lease storage for the vault client.
 *
 * @VaultLeaseStorage(
 *   id = "static",
 *   label = "Memory Array",
 *   description = @Translation("This lease storage uses PHP Static Memory."),
 * )
 */
final class VaultLeaseStaticStorage extends VaultLeaseStorageBase {

  /**
   * The storage handler.
   *
   * @var array
   */
  protected static array $storage = [];

  /**
   * VaultLeaseStateStorage constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface|null $client
   *   A non-lease-caching vault client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Component\Datetime\Time $time
   *   Time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ?VaultClientInterface $client, LoggerInterface $logger, protected Time $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $client, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    // During initial setup or if credentials are invalid we do not have a
    // client available.
    try {
      $vault_client = $container->get('vault.vault_client_no_lease_storage');
    }
    catch (\Exception $e) {
      $vault_client = NULL;
    }

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $vault_client,
      $container->get('logger.channel.vault'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeaseRaw(string $storage_key): ?array {
    // During renewAllLeases we will only know the hashed storage_key.
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $stored_lease = self::$storage[$storage_key] ?? NULL;
    if (
      empty($stored_lease)
      || !is_array($stored_lease)
      || empty($stored_lease['lease_id'])
      || empty($stored_lease['data'])
      || empty($stored_lease['expires'])
      || $stored_lease['expires'] <= $this->time->getCurrentTime()
      || empty($stored_lease['renewable'])
    ) {
      return NULL;
    }

    return $stored_lease;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllLeases(): array {
    $items = self::$storage;
    $returned = [];
    foreach ($items as $key => $item) {
      $returned[$key] = $item['data'];
    }
    return $returned;
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteLease(string $storage_key): void {
    unset(self::$storage[$storage_key]);
  }

  /**
   * {@inheritdoc}
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void {
    // During renewAllLeases we will only know the hashed storage_key.
    if (!str_starts_with($storage_key, 'hashed::')) {
      $storage_key = 'hashed::' . Crypt::hmacBase64($storage_key, Settings::get('hash_salt'));
    }
    $payload = [
      'lease_id' => $lease_id,
      'renewable' => $renewable,
      'data' => $data,
      'expires' => $this->time->getCurrentTime() + $expires,
    ];
    self::$storage[$storage_key] = $payload;
  }

}
