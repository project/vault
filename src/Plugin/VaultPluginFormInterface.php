<?php

namespace Drupal\vault\Plugin;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides an interface for plugins that have a configuration form.
 *
 * @api
 */
interface VaultPluginFormInterface extends PluginFormInterface {
}
