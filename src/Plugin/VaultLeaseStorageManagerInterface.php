<?php

namespace Drupal\vault\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Empty interface for use with mocking the Lease Storage Manager service.
 *
 * @internal
 */
interface VaultLeaseStorageManagerInterface extends PluginManagerInterface {

}
