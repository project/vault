<?php

namespace Drupal\vault\Plugin;

use Drupal\vault\VaultClientInterface;

/**
 * Class VaultLeaseStorage handles storage of leases in Drupal state api.
 *
 * @package Drupal\vault
 *
 * @api
 */
interface VaultLeaseStorageInterface {

  /**
   * Returns the data for a lease using Drupal's internal storage key.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return mixed
   *   The data stored in the lease. Null if no data can be returned.
   */
  public function getLease(string $storage_key): mixed;

  /**
   * Stores a new lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   * @param string $lease_id
   *   The lease ID.
   * @param mixed $data
   *   The lease data.
   * @param int $expires
   *   The lease expiry (relative to current time in seconds).
   * @param bool $renewable
   *   Is the lease renewable.
   */
  public function setLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void;

  /**
   * Updates the expiry of an existing lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   * @param int $new_expires
   *   The new lease expiry (relative to current time in seconds).
   * @param bool $renewable
   *   Is the renewed lease renewable.
   *
   * @return bool
   *   TRUE if lease updated, otherwise FALSE.
   */
  public function updateLeaseExpires(string $storage_key, int $new_expires, bool $renewable): bool;

  /**
   * Revoke an obtained lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return bool
   *   True if lease revoked, otherwise False.
   *
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function revokeLease(string $storage_key): bool;

  /**
   * Renew an obtained lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   * @param int $increment
   *   Number of seconds for lease to be valid.
   *
   * @return bool
   *   True if lease renewed successfully otherwise False.
   *
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function renewLease(string $storage_key, int $increment): bool;

  /**
   * Helper method to renew all existing leases.
   *
   * @param int $increment
   *   Additional length to request leases for. Should be _at least_ the number
   *   of seconds between cron runs.
   */
  public function renewAllLeases(int $increment): void;

  /**
   * Set a Vault Client to be used by the plugin.
   *
   * @param \Drupal\vault\VaultClientInterface $client
   *   The Vault Client to use for lease renewals.
   */
  public function setClient(VaultClientInterface $client): void;

}
