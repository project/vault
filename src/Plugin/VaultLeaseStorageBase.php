<?php

namespace Drupal\vault\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\vault\Exceptions\ClientException;
use Drupal\vault\VaultClientInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Vault Lease Storage plugins.
 *
 * @api
 */
abstract class VaultLeaseStorageBase extends PluginBase implements VaultLeaseStorageInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface|null $client
   *   A non-lease-caching vault client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ?VaultClientInterface $client, protected LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  abstract public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self;

  /**
   * Return all lease items.
   *
   * @return array
   *   Array of lease items.
   */
  abstract protected function getAllLeases(): array;

  /**
   * Returns the raw state entry for a lease from storage.
   *
   * Unlike getLease() this method accepts $storage_key being hashed.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return array|null
   *   The response from state api.
   *
   * @phpstan-return array{'lease_id': string, 'data': mixed, 'renewable': bool}|null
   */
  abstract protected function getLeaseRaw(string $storage_key): ?array;

  /**
   * Deletes a lease from storage.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   */
  abstract protected function deleteLease(string $storage_key): void;

  /**
   * {@inheritdoc}
   */
  public function getLease(string $storage_key): mixed {
    // Block attempts to use public method to obtain information about a lease
    // using hashed storage_key.
    if (str_starts_with($storage_key, 'hashed::')) {
      return NULL;
    }

    $item = $this->getLeaseRaw($storage_key);
    if (empty($item['data'])) {
      return NULL;
    }
    return $item['data'];
  }

  /**
   * Returns the lease ID for a given storage key.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return string|false
   *   The Vault lease ID.
   */
  protected function getLeaseId(string $storage_key): string|FALSE {
    $item = $this->getLeaseRaw($storage_key);
    if (empty($item)) {
      return FALSE;
    }
    return $item['lease_id'];
  }

  /**
   * Returns the renewable flag for a given storage key.
   *
   * @param string $storage_key
   *   The storage key. Something like "hashed::hash_lease_id".
   *
   * @return bool
   *   Is lease renewable.
   */
  protected function getIsRenewable(string $storage_key): bool {
    $item = $this->getLeaseRaw($storage_key);
    if (empty($item)) {
      return FALSE;
    }
    return $item['renewable'];
  }

  /**
   * {@inheritdoc}
   */
  public function updateLeaseExpires(string $storage_key, int $new_expires, bool $renewable): bool {
    $data = $this->getLeaseRaw($storage_key);
    if ($data == NULL) {
      return FALSE;
    }
    $this->setLease($storage_key, $data['lease_id'], $data['data'], $new_expires, $renewable);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLease(string $storage_key): bool {
    if ($this->client == NULL) {
      return FALSE;
    }
    $this->logger->debug("attempting to revoke lease for @key", ['@key' => $storage_key]);
    $lease_id = $this->getLeaseId($storage_key);
    if (empty($lease_id)) {
      $this->logger->error("could not find lease for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    $data["lease_id"] = $lease_id;
    $encoded_data = json_encode($data);

    if ($encoded_data === FALSE) {
      throw new ClientException("Failed to encode request to revoke lease for " . $storage_key);
    }

    try {
      $this->client->put($this->client->buildPath("/sys/leases/revoke"), $encoded_data);
    }
    catch (\Exception $e) {
      $this->logger->info("Failed revoking lease @lease_id", ['@lease_id' => $lease_id]);
      $this->deleteLease($lease_id);
      return FALSE;
    }

    $this->deleteLease($lease_id);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function renewLease(string $storage_key, int $increment): bool {
    if ($this->client == NULL) {
      return FALSE;
    }

    $this->logger->debug("attempting to renew lease for @key", ['@key' => $storage_key]);
    $lease_id = $this->getLeaseId($storage_key);

    if (empty($lease_id)) {
      $this->logger->error("no valid lease for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    if (!$this->getIsRenewable($storage_key)) {
      $this->logger->error("Attempt to renew non-renewable lease @key", ['@key' => $storage_key]);
      return FALSE;
    }

    $data = [
      "lease_id" => $lease_id,
      "increment" => $increment,
    ];

    $encoded_data = json_encode($data);
    if ($encoded_data === FALSE) {
      $this->logger->error("Failed to encode request to renew lease for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    try {
      $response = $this->client->put($this->client->buildPath("/sys/leases/renew"), $encoded_data);
    }
    catch (ClientExceptionInterface | \Exception $e) {
      $this->logger->error("Failed renewing lease @lease_id", ['@lease_id' => $lease_id]);
      return FALSE;
    }

    if (is_null($response->getRequestId())) {
      $this->logger->error("null response from server renewing lease for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    $new_expires = $response->getLeaseDuration();
    if ($new_expires === NULL) {
      $this->logger->error("null duration from server renewing lease for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    if ($response->isRenewable() == NULL) {
      $this->logger->error("Response missing renewable status for @key", ['@key' => $storage_key]);
      return FALSE;
    }

    if (!$this->updateLeaseExpires($storage_key, $new_expires, $response->isRenewable())) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function renewAllLeases(int $increment): void {
    if ($this->client == NULL) {
      $this->logger->error("Attempt to renew all leases with no client set");
      return;
    }

    $leases = $this->getAllLeases();
    foreach ($leases as $key => $value) {
      if (!$this->getIsRenewable($key)) {
        $this->logger->debug("Renewal skipped: @key is not renewable", ['@key' => $key]);
        continue;
      }

      $response = $this->renewLease($key, $increment);
      if (!$response) {
        // Failed to renew the lease - remove it from state.
        $this->logger->info("revoking expired lease for @key", ['@key' => $key]);
        $this->revokeLease($key);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setClient(VaultClientInterface $client): void {
    $this->client = $client;
  }

}
