<?php

namespace Drupal\vault;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriInterface;
use Vault\AuthenticationStrategies\AuthenticationStrategy;
use Vault\Builders\ResponseBuilder;
use Vault\Client;
use Vault\Models\Token;
use Vault\ResponseModels\Response;

/**
 * Methods below this are part of the vault-php library.
 *
 * We provide these here to fulfill the interface.
 * These may potentially be modified dependent upon upstream.
 *
 * PHPCS standards disabled as needed.
 */
interface VaultPhpInterface {

  // phpcs:disable Drupal.Commenting.FunctionComment.MissingParamComment
  // phpcs:disable Drupal.Commenting.DocComment
  // phpcs:disable MissingShort, Drupal.Commenting.FunctionComment.MissingReturnComment

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function head(string $path): Response;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function list(string $path = ''): Response;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function get(string $path = ''): Response;

  /**
   * @param string $path
   * @param string $body
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function put(string $path, string $body = ''): Response;

  /**
   * @param string $path
   * @param string $body
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function patch(string $path, string $body = ''): Response;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function options(string $path): Response;

  /**
   * @param string $path
   * @param string $body
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function post(string $path, string $body = ''): Response;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function delete(string $path): Response;

  /**
   * @return string
   */
  public function getVersion(): string;

  /**
   * @param string $version
   *
   * @return $this
   */
  public function setVersion(string $version);

  /**
   * @return \Vault\Models\Token
   */
  public function getToken(): Token;

  /**
   * @param string $namespace
   *
   * @return $this
   */
  public function setNamespace(string $namespace);

  /**
   * @return \Psr\Http\Message\UriInterface
   */
  public function getBaseUri(): UriInterface;

  /**
   * @param \Psr\Http\Message\UriInterface $baseUri
   *
   * @return $this
   */
  public function setBaseUri(UriInterface $baseUri);

  /**
   * @return \Psr\Http\Client\ClientInterface
   */
  public function getClient(): ClientInterface;

  /**
   * @param \Psr\Http\Client\ClientInterface $client
   *
   * @return $this
   */
  public function setClient(ClientInterface $client);

  /**
   * @return \Psr\Http\Message\RequestFactoryInterface
   */
  public function getRequestFactory(): RequestFactoryInterface;

  /**
   * @param \Psr\Http\Message\RequestFactoryInterface $requestFactory
   *
   * @return $this
   */
  public function setRequestFactory(RequestFactoryInterface $requestFactory);

  /**
   * @return \Psr\Http\Message\StreamFactoryInterface
   */
  public function getStreamFactory(): StreamFactoryInterface;

  // phpcs:disable Drupal.Commenting.FunctionComment.TypeHintMissing

  /**
   * @param \Psr\Http\Message\StreamFactoryInterface $streamFactory
   *
   * @return $this
   */
  public function setStreamFactory($streamFactory);

  // phpcs:enable Drupal.Commenting.FunctionComment.TypeHintMissing

  /**
   * @return \Vault\Builders\ResponseBuilder
   */
  public function getResponseBuilder(): ResponseBuilder;

  /**
   * @param \Vault\Builders\ResponseBuilder $responseBuilder
   *
   * @return $this
   */
  public function setResponseBuilder(ResponseBuilder $responseBuilder);

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function read(string $path): Response;

  /**
   * @return bool
   */
  public function isReadCacheEnabled(): bool;

  /**
   * @return $this
   */
  public function enableReadCache();

  /**
   * @return $this
   */
  public function disableReadCache();

  /**
   * @return int
   */
  public function getReadCacheTtl(): int;

  /**
   * @param int $readCacheTtl
   *
   * @return $this
   */
  public function setReadCacheTtl(int $readCacheTtl);

  /**
   * @param string $path
   *
   * @return string
   */
  public function buildPath(string $path): string;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function keys(string $path): Response;

  /**
   * @param string $path
   * @param array $data
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function write(string $path, array $data = []): Response;

  /**
   * @param string $path
   *
   * @return \Vault\ResponseModels\Response
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function revoke(string $path): Response;

  /**
   * @return \Psr\Cache\CacheItemPoolInterface
   */
  public function getCache(): CacheItemPoolInterface;

  /**
   * @param \Psr\Cache\CacheItemPoolInterface $cache
   *
   * @return \Vault\Client
   */
  public function setCache(CacheItemPoolInterface $cache): Client;

  /**
   * @return \Vault\AuthenticationStrategies\AuthenticationStrategy
   */
  public function getAuthenticationStrategy(): AuthenticationStrategy;

  /**
   * @param \Vault\AuthenticationStrategies\AuthenticationStrategy $authenticationStrategy
   *
   * @return \Vault\Client
   *
   */
  public function setAuthenticationStrategy(AuthenticationStrategy $authenticationStrategy): Client;

  /**
   * @inheritdoc
   * @throws \Vault\Exceptions\DependencyException
   * @throws \Vault\Exceptions\AuthenticationException
   * @throws \Exception
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function send(string $method, string $path, string $body = ''): ResponseInterface;

  /**
   * @return bool
   *
   * @throws \Vault\Exceptions\RuntimeException
   * @throws \Vault\Exceptions\DependencyException
   * @throws \Exception
   * @throws \InvalidArgumentException
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function authenticate(): bool;

  /**
   * @param \Vault\Models\Token $token
   *
   * @return $this
   */
  public function setToken(Token $token);

}
