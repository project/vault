<?php

declare(strict_types=1);

namespace Drupal\vault\Logging;

use Psr\Log\AbstractLogger;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Filters log messages from Vault\Client.
 *
 * @internal
 */
final class VaultLogLevelFilter extends AbstractLogger {

  /**
   * Log Levels.
   *
   * @var int[]
   */
  private const LEVELS = [
    LogLevel::DEBUG => 0,
    LogLevel::INFO => 1,
    LogLevel::NOTICE => 2,
    LogLevel::WARNING => 3,
    LogLevel::ERROR => 4,
    LogLevel::CRITICAL => 5,
    LogLevel::ALERT => 6,
    LogLevel::EMERGENCY => 7,
  ];

  /**
   * Minimum log level.
   *
   * @var int
   */
  protected int $logLevel;

  public function __construct(protected LoggerInterface $logger, string $log_level) {
    if (!array_key_exists($log_level, self::LEVELS)) {
      throw new \InvalidArgumentException("Log Level for " . __CLASS__ . " is not valid.");
    }
    $this->logLevel = self::LEVELS[$log_level];
  }

  /**
   * {@inheritDoc}
   */
  public function log($level, \Stringable|string $message, array $context = []): void {
    if (!is_string($level) && !(is_object($level) && is_a($level, \Stringable::class))) {
      throw new InvalidArgumentException("Unknown log level type passed to " . __FUNCTION__);
    }

    if (!is_string($level)) {
      $level = (string) $level;
    }

    if (!array_key_exists($level, self::LEVELS)) {
      throw new InvalidArgumentException("Log Level for " . __FUNCTION__ . " is unknown.");
    }

    if (self::LEVELS[$level] >= $this->logLevel) {
      $this->logger->log($level, $message, $context);
    }
  }

}
