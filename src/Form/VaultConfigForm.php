<?php

namespace Drupal\vault\Form;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\vault\Plugin\VaultAuthManagerInterface;
use Drupal\vault\Plugin\VaultLeaseStorageManagerInterface;
use Drupal\vault\Plugin\VaultPluginFormInterface;
use Drupal\vault\VaultCacheManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a config form for Encrypt KMS.
 */
final class VaultConfigForm extends ConfigFormBase {

  /**
   * Vault Auth Plugin Manager.
   *
   * @var \Drupal\vault\Plugin\VaultAuthManagerInterface
   */
  protected VaultAuthManagerInterface $authPluginManager;

  /**
   * Vault Lease Storage Plugin Manager.
   *
   * @var \Drupal\vault\Plugin\VaultLeaseStorageManagerInterface
   */
  protected VaultLeaseStorageManagerInterface $leasePluginManager;

  /**
   * Construct a new VaultConfigForm instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   Translation service.
   * @param \Drupal\vault\Plugin\VaultAuthManagerInterface $plugin_manager_auth
   *   Vault AUth Plugin Manager service.
   * @param \Drupal\vault\Plugin\VaultLeaseStorageManagerInterface $plugin_manager_lease_storage
   *   Vault Lease Storage Plugin Manager service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\vault\VaultCacheManager $vaultCacheManager
   *   The vault client cache manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $translation, VaultAuthManagerInterface $plugin_manager_auth, VaultLeaseStorageManagerInterface $plugin_manager_lease_storage, TypedConfigManagerInterface $typed_config_manager, protected VaultCacheManager $vaultCacheManager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->setStringTranslation($translation);
    $this->authPluginManager = $plugin_manager_auth;
    $this->leasePluginManager = $plugin_manager_lease_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('plugin.manager.vault_auth'),
      $container->get('plugin.manager.vault_lease_storage'),
      $container->get('config.typed'),
      $container->get('vault.cache.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['vault.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'vault.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('vault.settings');

    $form['base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Vault Server URL'),
      // MITM attacks being referenced: an attacker could compromise the Drupal
      // UI, reconfiguring this module to route all requests to a proxy under
      // their control. They could then steal any credentials passed over the
      // wire.
      '#description' => $this->t('Base URL of the vault server. You may consider hard-coding this value in settings.php to mitigate risk of MITM attacks.'),
      '#default_value' => $config->get('base_url'),
      '#required' => TRUE,
    ];

    $form['lease_ttl_increment'] = [
      '#type' => 'number',
      '#title' => $this->t('Lease TTL Increment'),
      '#description' => $this->t('Time (in seconds) that we request leases be extended for. Vault has to adhere to its own policy restrictions, the actual lease increment value may be lower. This number will depend on how frequently cron is being run.'),
      '#default_value' => $config->get('lease_ttl_increment'),
      '#required' => TRUE,
      '#size' => 16,
    ];

    $form['cache'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cache'),
      '#description' => $this->t('The Vault client caches the validated auth token, and optionally key reads, for increased performance. The default PHPArray Cache is non-persistent.'),
      '#description_display' => 'before',
    ];

    $form['cache']['read_cache_ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache TTL'),
      '#description' => $this->t('Time (in seconds) the Client will cache read request results. Use "0" to disable read-caching.'),
      '#default_value' => $config->get('read_cache_ttl') ?? 0,
      '#required' => TRUE,
      '#size' => 16,
    ];

    $form['cache']['clear_cache'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Cache'),
      '#description' => $this->t('Clear all entries from Vault client cache'),
      '#validate' => [
        [$this, 'cacheClearValidateForm'],
      ],
      '#submit' => [
        [$this, 'cacheClearSubmitForm'],
      ],
    ];

    $form['plugin_auth'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Strategy'),
      '#description' => $this->t('Select the authentication strategy you wish to use.'),
      '#default_value' => $config->get('plugin_auth'),
      '#empty_option' => $this->t('- Select Plugin -'),
      '#options' => [],
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxAuthPluginForm'],
        'wrapper' => 'vault-auth-plugin-config-form',
        'method' => 'replaceWith',
        'effect' => 'fade',
      ],
    ];

    // Load authentication strategy plugins and add their options to the form.
    $auth_plugin_manager = $this->authPluginManager;
    $plugin_definitions = $auth_plugin_manager->getDefinitions();
    foreach ($plugin_definitions as $id => $info) {
      // Add each plugin to the options list.
      $form['plugin_auth']['#options'][$id] = $info['label'];
    }

    // Auth Plugin configuration
    // Build the form.
    // Container to hold the lease_storage_plugin config forms.
    $form['plugin_auth_settings'] = [
      '#type' => 'container',
    ];
    $form['plugin_auth_settings']['#attributes']['id'] = 'vault-lease-storage-plugin-config-form';
    $form['plugin_auth_settings']['#tree'] = TRUE;

    $auth_plugin_to_create = $form_state->getValue('plugin_auth') ?? $config->get('plugin_auth');
    if (!empty($auth_plugin_to_create)) {
      $auth_plugin_config = $config->get('auth_plugin_config') ?: [];
      $auth_plugin = $auth_plugin_manager->createInstance($auth_plugin_to_create, $auth_plugin_config);
      if ($auth_plugin instanceof VaultPluginFormInterface) {
        $subform_state = SubformState::createForSubform($form['plugin_auth_settings'], $form, $form_state);
        $form['plugin_auth_settings'] = $auth_plugin->buildConfigurationForm($form['plugin_auth_settings'], $subform_state);

        // Modify the auth plugin configuration container element.
        $form['plugin_auth_settings']['#type'] = 'details';
        $form['plugin_auth_settings']['#title'] = $this->t('Authentication Plugin Settings');
        $form['plugin_auth_settings']['#open'] = TRUE;
      }
      $form_state->set('plugin_auth', $auth_plugin);
    }

    $form['plugin_auth_settings']['#attributes']['id'] = 'vault-auth-plugin-config-form';
    $form['plugin_auth_settings']['#tree'] = TRUE;

    $storage_manager = $this->leasePluginManager;
    $storage_plugins = [];
    foreach ($storage_manager->getDefinitions() as $id => $info) {
      $storage_plugins[$id] = $info['label'];
    }

    $current_lease_plugin = $config->get('plugin_lease_storage');
    $form['plugin_lease_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Lease Storage Strategy'),
      '#description' => $this->t('Select the lease storage strategy you wish to use.'),
      '#default_value' => $current_lease_plugin,
      '#options' => $storage_plugins,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxLeaseStorageForm'],
        'wrapper' => 'vault-lease-storage-plugin-config-form',
        'method' => 'replaceWith',
        'effect' => 'fade',
      ],
    ];

    // Container to hold the lease_storage_plugin config forms.
    $form['lease_storage_plugin_config'] = [
      '#type' => 'container',
    ];
    $form['lease_storage_plugin_config']['#attributes']['id'] = 'vault-lease-storage-plugin-config-form';
    $form['lease_storage_plugin_config']['#tree'] = TRUE;

    // Build the form.
    $plugin_to_create = $form_state->getValue('plugin_lease_storage') ?? $current_lease_plugin;
    $storage_plugin_config = $config->get('lease_storage_plugin_config') ?: [];
    $storage_plugin = $storage_manager->createInstance($plugin_to_create, $storage_plugin_config);

    if ($storage_plugin instanceof PluginFormInterface) {
      // Attach the plugin configuration form.
      $lease_storage_form_state = SubformState::createForSubform($form['lease_storage_plugin_config'], $form, $form_state);
      $form['lease_storage_plugin_config'] = $storage_plugin->buildConfigurationForm($form['lease_storage_plugin_config'], $lease_storage_form_state);

      // Modify the bucket plugin configuration container element.
      $form['lease_storage_plugin_config']['#type'] = 'details';
      $form['lease_storage_plugin_config']['#title'] = $this->t('Configure Lease Storage');
      $form['lease_storage_plugin_config']['#open'] = TRUE;
    }
    $form_state->set('plugin_lease_storage', $storage_plugin);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Handles switching form based on the selected auth plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxAuthPluginForm(array $form, FormStateInterface $form_state): array {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the plugin configuration form
    // based on that. So we just need to return the relevant part of the form
    // here.
    return $form['plugin_auth_settings'];
  }

  /**
   * Handles switching form based on the selected plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxLeaseStorageForm(array $form, FormStateInterface $form_state): array {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the plugin configuration form
    // based on that. So we just need to return the relevant part of the form
    // here.
    return $form['lease_storage_plugin_config'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $url = $form_state->getValue('base_url');
    if (!UrlHelper::isValid($url)) {
      $form_state->setError($form['base_url'], "Configured base URL is not valid");
    }

    $auth_plugin = $form_state->get('plugin_auth');

    $subform_state = SubformState::createForSubform($form['plugin_auth_settings'], $form, $form_state);
    if ($auth_plugin instanceof VaultPluginFormInterface) {
      $auth_plugin->validateConfigurationForm($form['plugin_auth_settings'], $subform_state);
    }

    $lease_storage_plugin = $form_state->get('plugin_lease_storage');

    if ($lease_storage_plugin instanceof PluginFormInterface) {
      $subform_lease_storage_state = SubformState::createForSubform($form['lease_storage_plugin_config'], $form, $form_state);
      $lease_storage_plugin->validateConfigurationForm($form['lease_storage_plugin_config'], $subform_lease_storage_state);
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->isRebuilding()) {
      return;
    }

    $config = $this->config('vault.settings');
    $config
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('plugin_auth', $form_state->getValue('plugin_auth'))
      ->set('lease_ttl_increment', $form_state->getValue('lease_ttl_increment'))
      ->set('plugin_lease_storage', $form_state->getValue('plugin_lease_storage'))
      ->set('read_cache_ttl', $form_state->getValue('read_cache_ttl'));

    $auth_plugin = $form_state->get('plugin_auth');
    if ($auth_plugin instanceof VaultPluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['plugin_auth_settings'], $form, $form_state);
      $auth_plugin->submitConfigurationForm($form['plugin_auth_settings'], $subform_state);
    }
    $auth_plugin_config = [];
    if ($auth_plugin instanceof ConfigurableInterface) {
      $auth_plugin_config = $auth_plugin->getConfiguration();
    }
    $config->set('auth_plugin_config', $auth_plugin_config);

    // Lease Storage Plugin config submit.
    /** @var \Drupal\vault\Plugin\VaultLeaseStorageInterface $lease_storage_plugin */
    $lease_storage_plugin = $form_state->get('plugin_lease_storage');
    if ($lease_storage_plugin instanceof PluginFormInterface) {
      $lease_storage_form_state = SubformState::createForSubform($form['lease_storage_plugin_config'], $form, $form_state);
      $lease_storage_plugin->submitConfigurationForm($form['lease_storage_plugin_config'], $lease_storage_form_state);
    }
    $lease_storage_plugin_config = [];
    if ($lease_storage_plugin instanceof ConfigurableInterface) {
      $lease_storage_plugin_config = $lease_storage_plugin->getConfiguration();
    }
    $config->set('lease_storage_plugin_config', $lease_storage_plugin_config);

    $config->save();
    $this->vaultCacheManager->clearCache();

    parent::submitForm($form, $form_state);
  }

  /**
   * NOOP validation handler.
   *
   * @param array $form
   *   Array that contains the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cacheClearValidateForm(array &$form, FormStateInterface $form_state): void {
    // NOOP.
  }

  /**
   * Clear the Vault client cache.
   *
   * @param array $form
   *   Array that contains the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cacheClearSubmitForm(array &$form, FormStateInterface $form_state): void {
    $clear_result = $this->vaultCacheManager->clearCache();
    if ($clear_result) {
      $this->messenger()->addStatus('Vault client cache cleared');
      return;
    }
    $this->messenger->addWarning('The cache was not fully cleared');
  }

}
