<?php

namespace Drupal\vault;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\PruneableInterface;

/**
 * Helper to manage the not-public 'vault.cache' service.
 */
final class VaultCacheManager {

  public function __construct(protected CacheItemPoolInterface $vaultClientCache) {}

  /**
   * Clear the Vault client cache.
   *
   * @return bool
   *   TRUE if cache was fully cleared, else FALSE.
   */
  public function clearCache(): bool {
    return $this->vaultClientCache->clear();
  }

  /**
   * Pune the cache of expired entries.
   *
   * @return bool
   *   TRUE on success or if cache does not implement PruneableInterface,
   *   else FALSE.
   */
  public function pruneCache(): bool {
    if (!is_a($this->vaultClientCache, PruneableInterface::class)) {
      // Cache does not support/require pruning.
      return TRUE;
    }
    return $this->vaultClientCache->prune();
  }

}
