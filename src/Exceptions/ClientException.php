<?php

namespace Drupal\vault\Exceptions;

/**
 * Class for exceptions related to errors in data returned by Vault Client.
 */
class ClientException extends \Exception {

}
