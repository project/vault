<?php

namespace Drupal\vault;

use Drupal\vault\Plugin\VaultAuthManagerInterface;
use Drupal\vault\Plugin\VaultLeaseStorageManagerInterface;
use GuzzleHttp\Psr7\Uri;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Vault\Exceptions\AuthenticationException;

/**
 * Factory class for Vault client.
 *
 * @package Drupal\vault
 */
final class VaultClientFactory {

  /**
   * Creates an Vault Client instance.
   *
   * @param \Drupal\vault\VaultConfigInterface $config
   *   The Vault config service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger service.
   * @param \Drupal\vault\Plugin\VaultAuthManagerInterface $vault_auth_manager
   *   Vault Auth Plugin Manager service.
   * @param \Drupal\vault\Plugin\VaultLeaseStorageManagerInterface $vault_lease_storage_manager
   *   Vault Lease Storage Plugin Manager service.
   * @param \Psr\Http\Client\ClientInterface $http_client
   *   PSR 17 http client.
   * @param \Psr\Http\Message\RequestFactoryInterface $request_factory
   *   PSR 17 request factory.
   * @param \Psr\Http\Message\StreamFactoryInterface $stream_factory
   *   Stream Factory.
   * @param \Psr\Cache\CacheItemPoolInterface $cache_pool
   *   Cache pool for Vault client.
   * @param bool $set_lease_storage
   *   True if factory should set lease storage. False if caller will be
   *   responsible to set storage or avoid storage.
   *
   * @return \Drupal\vault\VaultClient
   *   The client.
   *
   * @throws \Vault\Exceptions\AuthenticationException
   *   Thrown when the client is unable to authenticate.
   */
  public static function createInstance(VaultConfigInterface $config, LoggerInterface $logger, VaultAuthManagerInterface $vault_auth_manager, VaultLeaseStorageManagerInterface $vault_lease_storage_manager, ClientInterface $http_client, RequestFactoryInterface $request_factory, StreamFactoryInterface $stream_factory, CacheItemPoolInterface $cache_pool, bool $set_lease_storage): VaultClient {

    $base_uri = new Uri($config->getBaseUrl());
    $client = new VaultClient($base_uri, $http_client, $request_factory, $stream_factory, $logger);
    $read_cache_ttl = $config->getReadCacheTtl();
    if ($read_cache_ttl !== 0) {
      $client->enableReadCache();
      $client->setReadCacheTtl($read_cache_ttl);
    }
    $client->setCache($cache_pool);

    // Allow some clients to be created without a lease storage.
    if ($set_lease_storage) {
      $lease_plugin_name = $config->getLeasePluginName();
      $lease_plugin_config = $config->getLeasePluginConfig();

      /** @var \Drupal\vault\Plugin\VaultLeaseStorageInterface $lease_plugin */
      $lease_plugin = $vault_lease_storage_manager->createInstance($lease_plugin_name, $lease_plugin_config);
      $client->setLeaseStorage($lease_plugin);
    }

    // Load up auth strategy.
    try {
      $auth_plugin_name = $config->getAuthPluginName();
      if (empty($auth_plugin_name)) {
        throw new AuthenticationException("No auth plugin configured");
      }

      $auth_plugin_config = $config->getAuthPluginConfig();

      /** @var \Drupal\vault\Plugin\VaultAuthInterface $plugin */
      $plugin = $vault_auth_manager->createInstance($auth_plugin_name, $auth_plugin_config);
      $auth_strategy = $plugin->getAuthenticationStrategy();

      $authenticated = $client->setAuthenticationStrategy($auth_strategy)->authenticate();
      if (!$authenticated) {
        throw new AuthenticationException("Failed to authenticate");
      }
    }
    catch (\Exception $e) {
      $logger->error(sprintf("[%s] %s", get_class($e), $e->getMessage()));
      throw $e;
    }

    return $client;
  }

}
