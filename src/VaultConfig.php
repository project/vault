<?php

declare(strict_types=1);

namespace Drupal\vault;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Vault config.
 *
 * @internal
 *   There is no extensibility promise for this class. Use service decorators to
 *   customize.
 */
final class VaultConfig implements VaultConfigInterface {

  /**
   * Constructs a new AuditFilesConfig.
   */
  final public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl(): string {
    $base_url = $this->configFactory->get('vault.settings')->get('base_url');
    if (empty($base_url) || !is_string($base_url)) {
      $base_url = 'https://vault:8200';
    }
    return $base_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthPluginName(): string|NULL {
    $plugin_name = $this->configFactory->get('vault.settings')->get('plugin_auth');
    if (empty($plugin_name) || !is_string($plugin_name)) {
      return NULL;
    }
    return $plugin_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthPluginConfig(): array {
    $config = $this->configFactory->get('vault.settings')->get('auth_plugin_config');
    if (!is_array($config)) {
      return [];
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getCronRenewEnabled(): bool {
    $enable_cron = $this->configFactory->get('vault.settings')->get('lease_renew_cron');
    if ($enable_cron !== TRUE) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeasePluginName(): string {
    $plugin_name = $this->configFactory->get('vault.settings')->get('plugin_lease_storage');
    if (!is_string($plugin_name) || empty($plugin_name)) {
      $plugin_name = 'state';
    }
    return $plugin_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeasePluginConfig(): array {
    $plugin_config = $this->configFactory->get('vault.settings')->get('lease_storage_plugin_config');
    if (!is_array($plugin_config)) {
      return [];
    }
    return $plugin_config;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeaseTtlIncrement(): int {
    $ttl_increment = $this->configFactory->get('vault.settings')->get('lease_ttl_increment');
    if (!is_int($ttl_increment) || $ttl_increment <= 0) {
      return 86400;
    }
    return $ttl_increment;
  }

  /**
   * {@inheritdoc}
   */
  public function getReadCacheTtl(): int {
    $read_cache_ttl = $this->configFactory->get('vault.settings')->get('read_cache_ttl');
    if (!is_int($read_cache_ttl) || $read_cache_ttl < 0) {
      return 0;
    }
    return $read_cache_ttl;
  }

}
