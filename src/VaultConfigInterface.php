<?php

declare(strict_types=1);

namespace Drupal\vault;

/**
 * Vault config interface.
 */
interface VaultConfigInterface {

  /**
   * Get the url of the Vault server.
   */
  public function getBaseUrl(): string;

  /**
   * Get the name of the Auth plugin.
   */
  public function getAuthPluginName(): string|NULL;

  /**
   * Get the auth plugin config.
   */
  public function getAuthPluginConfig(): array;

  /**
   * Get if leases should be renewed during cron.
   */
  public function getCronRenewEnabled(): bool;

  /**
   * Get the lease storage plugin.
   */
  public function getLeasePluginName(): string;

  /**
   * Get the lease storage plugin config.
   */
  public function getLeasePluginConfig(): array;

  /**
   * Get the time to increment TTL during renewal.
   */
  public function getLeaseTtlIncrement(): int;

  /**
   * Get the TTL for storing items in read cache.
   *
   * @phpstan-return int<0, max>
   */
  public function getReadCacheTtl(): int;

}
