<?php

namespace Drupal\vault;

use Drupal\vault\Plugin\VaultLeaseStorageInterface;

/**
 * Wrapper for \Vault\Client providing some helper methods.
 *
 * It also acts as a translation layer between vault leases and drupal entities
 * implementing leases (like keys).
 *
 * @api
 */
interface VaultClientInterface extends VaultPhpInterface {

  /**
   * Sets the leaseStorage property.
   *
   * @param \Drupal\vault\Plugin\VaultLeaseStorageInterface $leaseStorage
   *   The lease storage object.
   */
  public function setLeaseStorage(VaultLeaseStorageInterface $leaseStorage): void;

  /**
   * Queries list of secret engine mounts on the configured vault instance.
   *
   * @return array|null
   *   Response from vault server.
   */
  public function listMounts(): ?array;

  /**
   * Queries list of particular secret backends.
   *
   * @param array $engine_types
   *   Array of secret engine types to list.
   *
   * @return array
   *   Array of secret engine mounts.
   */
  public function listSecretEngineMounts(array $engine_types): array;

  /**
   * Stores a lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   * @param string $lease_id
   *   The lease ID.
   * @param mixed $data
   *   The lease data.
   * @param int $expires
   *   The lease expiry (relative to current time in seconds).
   * @param bool $renewable
   *   Is the lease renewable.
   */
  public function storeLease(string $storage_key, string $lease_id, mixed $data, int $expires, bool $renewable): void;

  /**
   * Retrieve a lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return mixed
   *   The data stored in the lease.
   */
  public function retrieveLease(string $storage_key): mixed;

  /**
   * Revoke an obtained lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   *
   * @return bool
   *   True if lease revoked, otherwise False.
   */
  public function revokeLease(string $storage_key): bool;

  /**
   * Renew an obtained lease.
   *
   * @param string $storage_key
   *   The storage key. Something like "key:key_machine_id".
   * @param int $increment
   *   Number of seconds for lease to be valid.
   *
   * @return bool
   *   True if lease renewed successfully otherwise False.
   */
  public function renewLease(string $storage_key, int $increment): bool;

  /**
   * Helper method to renew all existing leases.
   *
   * @param int $increment
   *   Additional length to request leases for. Should be _at least_ the number
   *   of seconds between cron runs.
   */
  public function renewAllLeases(int $increment): void;

}
