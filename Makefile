#!/usr/bin/make -f

DOCS_DST=public
LISTEN?=localhost:8000
MKDOCS=mkdocs
PIP=pip3

.DEFAULT_GOAL := docs-serve

docs-install-dep:
	$(PIP) install mkdocs-material mkdocs-literate-nav
docs-serve:
	$(MKDOCS) serve -a $(LISTEN) --watch-theme -w mkdocs.yml -w README.md -w docs
docs-build:
	$(MKDOCS) build --site-dir $(DOCS_DST)
docs-clean:
	rm -Rf $(DOCS_DST)

clean: docs-clean

.PHONY: docs-install-dep docs-serve docs-build docs-clean clean
